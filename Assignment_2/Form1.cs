﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolarHrmData_Assignment1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        

        //Declaring Array for storing different line data into it
        private string[] LinesOfFile;

        //Creating list and object for different class
        List<header> headingValue = new List<header>();
        public List<hrdata> hr = new List<hrdata>();
        //List<parameters> parameterValue = new List<parameters>();
        parameters allParameters = new parameters();


        int lineNo; //line numbers value


        //datatables for the values

        DataTable dt = new DataTable();
        
        private void Form1_Load(object sender, EventArgs e)
        {
            cycleInfo.Visible = false;
            label7.Visible = false;
            label8.Visible = false;
            label9.Visible = false;
            label10.Visible = false;
            label11.Visible = false;
            label12.Visible = false;
            label13.Visible = false;
            label14.Visible = false;
            label15.Visible = false;
        }

        private void browseFile_Click_1(object sender, EventArgs e)
        {
            using (OpenFileDialog browseFile = new OpenFileDialog())
            {
                if (browseFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string filename = browseFile.FileName;
                    //Reading the File
                    MessageBox.Show("."+filename);

                    LinesOfFile = File.ReadAllLines(filename);
                    
                    //Executing the loop only when the file is not empty.
                    for (int linecount = 0; linecount < LinesOfFile.Length; linecount++)
                    {
                        if (LinesOfFile[linecount].Length > 0)
                        {
                            //Finding The Header and assigning it to Header Class, Here header is between []
                            if (LinesOfFile[linecount][0] == '[' && LinesOfFile[linecount][LinesOfFile[linecount].Length - 1] == ']')
                            {
                                //Assigning Headername to header list
                                headingValue.Add(new header { headername = LinesOfFile[linecount].Substring(1, LinesOfFile[linecount].Length - 2), headerline = linecount });
                                cycleInfo.Visible = true;
                                label7.Visible = true;
                                label8.Visible = true;
                                label9.Visible = true;
                                label10.Visible = true;
                                label11.Visible = true;
                                label12.Visible = true;
                                label13.Visible = true;
                                label14.Visible = true;
                                label15.Visible = true;
                            }
                        }
                    }


                }
                //calling method to store particular set of data in objects

                fileParse();

            }
        }

        public void fileParse()
        {
            //checking the filelist array with HRData header
            foreach (header a in headingValue)
            {
                lineNo = a.headerline;

                //checking the condition
                switch (a.headername)
                {
                    case "Params":
                        {
                            int nextLine = lineNo + 1;
                            //seperating the values with tabs
                            string[] newline = LinesOfFile[nextLine].Split('\t');
                            string value = newline[0];
                            int add = 0;
                            int addval = 1;
                            string[] paramsValue = new string[22];
                            do
                            {
                                
                                foreach (char checkequal in value)
                                {
                                    if (checkequal == '=')
                                    {
                                        paramsValue[add] = value.Substring(addval, value.Length - addval);
                                        
                                    }

                                    addval++;
                                }

                                
                                addval = 1;
                                add++;
                                nextLine++;
                                newline = LinesOfFile[nextLine].Split('\t');
                                value = newline[0];

                            } while (nextLine < 24);//newline!=null);

                           
                            allParameters.Version = paramsValue[0];
                            allParameters.Monitor = paramsValue[1];
                            allParameters.SMode = paramsValue[2];
                            allParameters.Date = paramsValue[3];
                            allParameters.StartTime = paramsValue[4];
                            allParameters.Length = paramsValue[5];
                            allParameters.Interval = paramsValue[6];
                            allParameters.Upper1 = paramsValue[7];
                            allParameters.Lower1 = paramsValue[8];
                            allParameters.Upper2 = paramsValue[9];
                            allParameters.Lower2 = paramsValue[10];
                            allParameters.Upper3 = paramsValue[11];
                            allParameters.Lower3 = paramsValue[12];
                            allParameters.Timer1 = paramsValue[13];
                            allParameters.Timer2 = paramsValue[14];
                            allParameters.Timer3 = paramsValue[15];
                            allParameters.ActiveLimit = paramsValue[16];
                            allParameters.MaxHR = paramsValue[17];
                            allParameters.RestHR = paramsValue[18];
                            allParameters.StartDelay = paramsValue[19];
                            allParameters.VO2max = paramsValue[20];
                            allParameters.Weight = paramsValue[21];


                            //adding the getter and setter
                            label16.Text = "Version: " + allParameters.Version;
                            label17.Text = "Monitor: " + allParameters.Monitor;
                            label18.Text = "SMode: " + allParameters.SMode;
                            label19.Text = "Date: " + allParameters.Date;
                            label20.Text = "Start Time: " +allParameters.StartTime;
                            label21.Text = "Length: " + allParameters.Length;
                            label22.Text = "Interval: " + allParameters.Interval;
                            label23.Text = "Upper1: " + allParameters.Upper1;
                            label24.Text = "Upper2: " + allParameters.Upper2;
                            label25.Text = "Upper3: " + allParameters.Upper3;
                            label26.Text = "Lower1: " + allParameters.Lower1;
                            label27.Text = "Lower2: " + allParameters.Lower2;
                            label28.Text = "Lower3: " + allParameters.Lower3;
                            label29.Text = "Timer1: " + allParameters.Timer1;
                            label31.Text = "Timer2: " + allParameters.Timer2;
                            label32.Text = "Timer3: " + allParameters.Timer3;
                            label33.Text = "ActiveLimit: " + allParameters.ActiveLimit;
                            label34.Text = "MaxHR: " + allParameters.MaxHR;
                            label35.Text = "RestHR: " + allParameters.RestHR;
                            label36.Text = "StartDelay: " + allParameters.StartDelay;
                            label37.Text = "VO2max: " + allParameters.VO2max;
                            label38.Text = "Weight: " + allParameters.Weight;

                            

                            break;
                        }
                    case "HRData":
                        {


                            //using the loop to till the end of array to get values 
                            for (int j = lineNo + 1; j < LinesOfFile.Length; j++)
                            {
                                //Spliting chars with tabs
                                string[] newline = LinesOfFile[j].Split('\t');


                                //Switching different versions 
                                switch (allParameters.Version)
                                {

                                    case "105":
                                        {
                                            hr.Add(new hrdata
                                            {
                                                HeartRate = int.Parse(newline[0]),
                                                Speed = int.Parse(newline[1]),
                                                Cadence = int.Parse(newline[2])
                                            });

                                            break;
                                        }
                                    case "106":
                                        {


                                            //adding the values 
                                            hr.Add(new hrdata
                                            {
                                                HeartRate = int.Parse(newline[0]),
                                                Speed = double.Parse(newline[1]) * 0.1,
                                                Cadence = int.Parse(newline[2]),
                                                Altitude = int.Parse(newline[3]),
                                                Power = int.Parse(newline[4]),
                                                PowerBalancePedalling = int.Parse(newline[5])
                                            });

                                            break;
                                        }
                                    case "107":
                                        {
                                            //adding the values to the parameters
                                            hr.Add(new hrdata
                                            {
                                                HeartRate = int.Parse(newline[0]),
                                                Speed = double.Parse(newline[1]) * 0.1,
                                                Cadence = int.Parse(newline[2]),
                                                Altitude = int.Parse(newline[3]),
                                                Power = int.Parse(newline[4]),
                                                PowerBalancePedalling = int.Parse(newline[5]),
                                                AirPressure = int.Parse(newline[6])
                                            });
                                            break;
                                        }
                                }
                            }
                            break;
                        }

                }
            }
            

            //putting column name is data grid view as per version of parmas
            string[] columnNames = { " HeartRate", " Speed", " Cadence", " Altitude", " Power", "PowerBalancePedalling" };


            foreach (string col in columnNames)
            {
                dt.Columns.Add(col);
            }


            foreach (hrdata hd in hr)
            {



                dt.Rows.Add(hd.HeartRate, hd.Speed, hd.Cadence, hd.Altitude, hd.Power, hd.PowerBalancePedalling);
            }

            //variables initiated for heart rate
            int minHeartRate = 1000;
            int maxHeartRate = 0, sum = 0;
            int count = 0;

            //variables for altitude ,power,speed

            int minAltitude = 1000;
            int maxAltitude = 0;
            int minPower = 1000;
            int maxPower = 0;
            double minSpeed = 1000;
            double maxSpeed = 0;
            //calculating the data to find maximum,minimum

            foreach (hrdata value in hr)
            {
                //for heart rate
                int hrValue = value.HeartRate;
                if (hrValue < minHeartRate)
                {
                    minHeartRate = hrValue;
                }
                else if (hrValue > maxHeartRate)
                {
                    maxHeartRate = hrValue;
                }
                sum += hrValue;
                count++;
                //for altitude 
                int altValue = value.Altitude;
                if (altValue < minAltitude)
                {
                    minAltitude = altValue;
                }
                else if (altValue > maxAltitude)
                {
                    maxAltitude = altValue;
                }

                //for power
                int PowerValue = value.Power;
                if (PowerValue < minPower)
                {
                    minPower = PowerValue;
                }
                else if (altValue > maxPower)
                {
                    maxPower = PowerValue;
                }
                //speed

                double SpeedValue = value.Speed;
                if (SpeedValue < minSpeed)
                {
                    minSpeed = SpeedValue;
                }
                else if (SpeedValue > maxSpeed)
                {
                    maxSpeed = SpeedValue;
                }


            }
            //calculating the stats of heart rate
            label7.Text ="Average Heart Rate::"+ (sum / count).ToString();
            label8.Text ="Maximum Heart Rate::"+maxHeartRate.ToString();
            label9.Text = "Minimum Heart Rate::" + minHeartRate.ToString();
            
            //calculating for altitude ,power ,speed
            label10.Text ="Maximun Power::" + maxPower.ToString()+"Watt";
            label11.Text ="Minimum Power::" +minPower.ToString()+"Watt";

            label12.Text = "Maximum Altitude::" + maxAltitude.ToString()+"Meter";
            label13.Text = "Minimum Altitude::" + minAltitude.ToString()+"Meter";

            label14.Text = "Maximum Speed::" + maxSpeed.ToString()+"Km/Hr";
            label15.Text = "Minimum Speed::" + minSpeed.ToString() + "Km/Hr";


            cycleInfo.DataSource = dt;
        }
        public void heartrate()
        {
            


        }

        private void ParseData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void ParseData_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void staticGraphToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            graph gr = new graph();
            gr.setHR(hr);
            gr.Show();
        }

        private void tabControl1_Click(object sender, EventArgs e)
        {

        }

        //public List<hrdata> getHRDataList()
        //{
        //    return hr;
        //}
   
Google
About 21,000,000 results (0.50 seconds) 
Search Results
The syntax to create a function in Oracle is: CREATE [OR REPLACE] FUNCTION function_name [ (parameter [,parameter]) ] RETURN return_datatype IS. AS [declaration_section] BEGIN executable_section [EXCEPTION exception_section] END [function_name]; When you create a procedure or function, you may define parameters.
Oracle / PLSQL: Functions - TechOnTheNet
https://www.techonthenet.com/oracle/functions.php
Feedback
About this result
People also ask
What is the function in PL SQL?
What is the function of a character?
What is the use of packages in PL SQL?
How do I create a function in Excel?

Feedback
Oracle / PLSQL: Functions - TechOnTheNet
https://www.techonthenet.com/oracle/functions.php

The syntax to create a function in Oracle is: CREATE [OR REPLACE] FUNCTION function_name [ (parameter [,parameter]) ] RETURN return_datatype IS. AS [declaration_section] BEGIN executable_section [EXCEPTION exception_section] END [function_name]; When you create a procedure or function, you may define parameters.
CREATE FUNCTION
https://docs.oracle.com/cd/B19306_01/server.102/b14200/statements_5009.htm

The return value can have any datatype supported by PL/SQL. Note: Oracle SQL does not support calling of functions with Boolean parameters or returns. ... Oracle Database derives the length, precision, or scale of the return value from the environment from which the function is called.
5 Using Procedures, Functions, and Packages - Oracle Help Center
https://docs.oracle.com/cd/B25329_01/doc/appdev.102/.../xedev_programs.htm

Stored procedures and functions (subprograms) can be compiled and stored in an Oracle Database XE, ready to be executed. Once compiled, it is a schema object known as a stored procedure or stored function, which can be referenced or called any number of times by multiple applications connected to Oracle Database ...
‎Overview of Procedures ... · ‎Managing Stored ... · ‎Oracle Provided Packages
You visited this page on 2/5/18.
CREATE FUNCTION Statement
https://docs.oracle.com/cloud/latest/db112/LNPLS/create_function.htm

To embed a CREATE FUNCTION statement inside an Oracle precompiler program, you must terminate the statement with the keyword END-EXEC followed by the embedded SQL statement terminator for the specific language. See Also: For more information about such prerequisites: Oracle Database Advanced Application ...
SQL Functions - Oracle Help Center
https://docs.oracle.com/cd/B19306_01/server.102/b14200/functions001.htm

If you call a SQL function with an argument of a datatype other than the datatype expected by the SQL function, then Oracle attempts to convert the argument to the expected datatype before performing the SQL function. If you call a SQL function with a null argument, then the SQL function automatically returns null. The only ...
Oracle Functions - W3Schools
https://www.w3schools.com/sql/sql_ref_oracle.asp

Function, Description. ASCII, Returns the number code that represents the specified character. ASCIISTR, Converts a string in any character set to an ASCII string using the database character set. CHR, Returns the character based on the number code. COMPOSE, Returns a Unicode string. CONCAT, Allows you to ...
PL/SQL function examples - Burleson Consulting
www.dba-oracle.com/t_plsql_function_examples.htm

PL/SQL function examples. ... Oracle PL/SQL Tips by Donald BurlesonMarch 18, 2015. Question: I want some examples of the PL/SQL functions syntax. Answer: PL/SQL functions allow only INPUT parameters and they must return a value. Here is a PL/SQL function example to change Fahrenheit temperatures to centigrade:.
CREATE FUNCTION - Examples - Oracle Wiki - Oracle - Toad World
https://www.toadworld.com/platforms/oracle/w/wiki/4205.create-function-examples

Mar 13, 2013 - It is suggested to use the CREATE OR REPLACE command to create functions. This allows replacement of existing triggers. To ensure you don't replace a FUNCTION that is already in your schema, query the USER_TRIGGERS view before issuing the CREATE OR REPLACE FUNCTION command.
Oracle PL/SQL – CREATE function example - Mkyong
https://www.mkyong.com/oracle/oracle-plsql-create-function-example/

Oracle PL/SQL – CREATE function example. By Dhaval Dadhaniya | August 30, 2017 | Viewed : 11,504 times +658 pv/w. This article will help you to understand how to create a user defined function. It's also known as stored function or user function. User defined functions are similar to procedures. The only difference is ...
Examples for Creating Oracle Functions - CodeProject
https://www.codeproject.com › Database › Database

Mar 13, 2011 - Even though some of the functions are just short 'aliases' to native Oracle functions, I wanted to create a bit more intuitive versions. Most of the functions are defined as DETERMINISTIC meaning that the function always produces the same result if the input values are the same. So these functions can be ...
Searches related to function in oracle

procedures and functions in oracle with examples

how to call a function in pl/sql

how to call a function in oracle

user defined functions in oracle

oracle function example return varchar2

oracle function example return multiple values

oracle functions pdf

pl sql procedures and functions with example pdf
	1	
2
	
3
	
4
	
5
	
6
	
7
	
8
	
9
	private void data_selection_Load(object sender, EventArgs e)
        {
            GraphPane myPane = zedGraphControl1.GraphPane;

            // Setting the Titles
            myPane.Title.Text = "Graph Analytics for Speed, Cadence, Altitude, Heart Rate and Power";
            myPane.XAxis.Title.Text = "Time in seconds";
            myPane.YAxis.Title.Text = "Individual Units of Measurements";

            int[] heart_rate;
            int[] speed;
            int[] cadence;
            int[] altitude;
            int[] power;

            dataselect.Columns.Add("SNo", "S.No.");
            dataselect.Columns.Add("HR", "Heart");
            dataselect.Columns.Add("SPD", "Speed");
            dataselect.Columns.Add("CAD", "Cadence");
            dataselect.Columns.Add("ALT", "Altitude");
            dataselect.Columns.Add("PWR", "Power");

            try
            {
                string txtData = File.ReadAllText(file_name);
                string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                int index2 = Array.IndexOf(arrData, "[HRData]");
                heart_rate = new int[arrData.Length - (index2 + 1)];
                speed = new int[arrData.Length - (index2 + 1)];
                cadence = new int[arrData.Length - (index2 + 1)];
                altitude = new int[arrData.Length - (index2 + 1)];
                power = new int[arrData.Length - (index2 + 1)];

                int j = 0;
                double total = 0;
                double hr = 0;
                double pwrtotal = 0;
                double alt = 0;
                double sno = 1;


                for (int i = index2 + 1; i < arrData.Length; i++)
                {
                    string HRData = arrData[i];
                    string[] arrHrdata = Regex.Split(HRData, @"\W+");

                    heart_rate[j] = Convert.ToInt32(arrHrdata[0]);
                    speed[j] = Convert.ToInt32(arrHrdata[1]);
                    cadence[j] = Convert.ToInt32(arrHrdata[2]);
                    altitude[j] = Convert.ToInt32(arrHrdata[3]);
                    power[j] = Convert.ToInt32(arrHrdata[4]);

                    // for Average speed
                    double spd = Convert.ToDouble(arrHrdata[1]) / 10;
                    double xa = speed[j];
                    total = total + xa;

                    //Average heart Rate
                    double hrt = heart_rate[j];
                    hr = hrt + hrt;

                    //Average power
                    double pwr = power[j];
                    pwrtotal = pwrtotal + pwr;

                    double altd = altitude[j];
                    alt = alt + altd;

                    // Display data in DataGrid view
                    dataselect.Rows.Add(new object[] { sno++, arrHrdata[0], spd.ToString(), cadence[j], altitude[j], power[j] });
                    j++;
                }


            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataselect_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            int i = 0, count = dataselect.SelectedRows.Count;

            string[] heart_rate = new string[count];
            string[] speed = new string[count];
            string[] cadence = new string[count];
            string[] altitude = new string[count];
            string[] power = new string[count];

            foreach (DataGridViewRow row in dataselect.SelectedRows)
            {
                heart_rate[i] = row.Cells[1].Value.ToString();
                speed[i] = row.Cells[2].Value.ToString();
                cadence[i] = row.Cells[3].Value.ToString();
                altitude[i] = row.Cells[4].Value.ToString();
                power[i] = row.Cells[5].Value.ToString();

                i++;
            }

            PointPairList heartlist = new PointPairList();
            PointPairList speedlist = new PointPairList();
            PointPairList cadencelist = new PointPairList();
            PointPairList altitudelist = new PointPairList();
            PointPairList powerlist = new PointPairList();

            for (int j = 0; j < count; j++)
            {
                heartlist.Add(j, Double.Parse(heart_rate[j]));
                speedlist.Add(j, Double.Parse(speed[j]));
                cadencelist.Add(j, Double.Parse(cadence[j]));
                altitudelist.Add(j, Double.Parse(altitude[j]));
                powerlist.Add(j, Double.Parse(power[j]));

            }

            ZedGraphControl zedGraph = zedGraphControl1;
            GraphPane mypane = zedGraph.GraphPane;

            mypane.CurveList.Clear();

            mypane.XAxis.Title.Text = "Time";
            mypane.XAxis.Scale.Min = 0;
            mypane.XAxis.Scale.Max = count;


            LineItem Curve1 = mypane.AddCurve("Heart rate", heartlist, Color.Red);
            LineItem Curve2 = mypane.AddCurve("Speed", speedlist, Color.Blue);
            LineItem Curve3 = mypane.AddCurve("Cadence", cadencelist, Color.Green);
            LineItem Curve4 = mypane.AddCurve("Altitude", altitudelist, Color.Yellow);
            LineItem Curve5 = mypane.AddCurve("Power", powerlist, Color.Black);


            zedGraph.AxisChange();
            zedGraph.Invalidate();
        }

        private void dataselect_MouseUp(object sender, MouseEventArgs e)
        {

        }
    }
}

10
	
Next
Nepal Patan - From your search history - Use precise location - Learn more
HelpSend feedbackPrivacyTerms

    }

}

using System.Threading.Tasks;
using System.Windows.Forms;

namespace KYC_form
{
    public partial class login : Form
    {
        public login()
        {
            InitializeComponent();
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            if (txtusername.Text == "admin" && txtusername.Text == "admin")
            {
                DashBoard dashboard = new DashBoard();
                dashboard.Show();
            }
            else if (txtusername.Text == "" || txtpassword.Text == "")
            {
                if (txtusername.Text == "")
                {
                    MessageBox.Show("Username is empty");
                    txtusername.Focus();
                }
                else
                {
                    MessageBox.Show("Password is empty");
                    txtpassword.Focus();
                }
            }
            else if (txtusername.Text != "admin" || txtusername.Text == "admin")
            {
                MessageBox.Show("Incorrect Username and Password Combination");
            }
            
        }



        private void check_Load(object sender, EventArgs e)
        {
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
    using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace read_write
{
    public partial class Form1 : Form
    {
        string nameFile;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            //open.ShowDialog();

            if (open.ShowDialog() == DialogResult.OK)
            { 
                StreamReader r = new StreamReader(open.FileName);
                textBox1.Text =  r.ReadToEnd();
                nameFile = open.FileName;
                r.Close();
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
                //Pass the filepath and filename to the StreamWriter Constructor
                StreamWriter sw = new StreamWriter(nameFile);

                //Write a line of text
                sw.WriteLine(textBox1.Text);

                //Close the file
                sw.Close();
           
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace txt_to_dgv
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
                OpenFileDialog open1 = new OpenFileDialog();

                if (open1.ShowDialog() == DialogResult.OK)
                {

                    string fileName = open1.FileName;
                    StreamReader filereader = new StreamReader(open1.FileName);

                    string s = filereader.ReadToEnd();
                }

                DataTable dt = new DataTable();
                dt.Columns.Add("Name");
                dt.Columns.Add("RollNo");
                dt.Columns.Add("Address");
                dt.Columns.Add("Mobile");

                StreamReader fileReader = new StreamReader(open1.FileName);

                while (!fileReader.EndOfStream)
                {
                    string line = fileReader.ReadLine();
                    string[] lineColumn = line.Split();
                    dt.Rows.Add(lineColumn);
                }
                dataGridView1.DataSource = dt;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        create or replace procedure pro_consultant(v_consultant in varchar2,v_username in varchar2,v_password in varchar2)
 is
v_date varchar2(20);
v_user varchar2(20);
v_userid number:=seq_cun.nextval;
begin
select v('app_user'),to_char(sysdate,'dd/mm/yyyy') into v_user,v_date from dual;

execute immediate 'insert into lds_consultant (consultant_id,cst_name,cst_start_date,create_by,create_date) values (:a,:b,:c,:d,:e)'
using v_userid,v_consultant,sysdate,v_user,v_date;

 execute immediate ' create user '||v_username||' identified by '||v_password;
 execute immediate 'insert into application_users values(:x,:y,:z)' using v_userid ,v_username,v_password;

 execute immediate ' grant create session,resource,connect to '||v_username||' with admin option';
end;?

create or replace procedure delete_consultant(v_consultant in varchar2)
 is
 begin
execute immediate ' delete from lds_consultant where cst_name= :a' using v_consultant;
execute immediate ' delete from application_users where user_id =(select consultant_id from lds_consultant where cst_name =:a)' using v_consultant;

 end;?


create or replace procedure get_datas(table_name varchar2) is
type data is ref cursor;
v_data data;
emprec empl%rowtype;
deptrec empl%rowtype;
v_stmt varchar2(1000);
begin
v_stmt:='select * from '||table_name;
open v_data for v_stmt;
loop
fetch v_data into emprec;
exit when v_data%notfound;
dbms_output.put_line(emprec.name);
end loop;
close v_data;
end;?


create or replace procedure insert_users is
     type crr is ref cursor;
    v_cur crr;
    v_name varchar2(25);
    v_id  number;
    begin
     open v_cur for  select consultant_id, substr(cst_name,1,instr(cst_name,' '))  from lds_consultant where consultant_id between 1 and 13;
    loop
    fetch v_cur into v_id,v_name;
    exit when v_cur%notfound;
   execute immediate 'insert into application_users(user_id,username,password) values(:a,:b,:c)' using v_id,v_name,v_name;
   end loop;
   close v_cur;
   end;?


SELECT 'GRANT SELECT ON LDS_ACCOUNT TO '|| USERNAME||';'
  FROM APPLICATION_USERS JOIN LDS_CONSULTANT ON APPLICATION_USERS.USER_ID= LDS_CONSULTANT.CONSULTANT_ID
   WHERE (TO_DATE(SYSDATE)-TO_DATE(CST_START_DATE))/365<1;
        create or replace procedure pro_consultant(v_consultant in varchar2,v_username in varchar2,v_password in varchar2)
 is
v_date varchar2(20);
v_user varchar2(20);
v_userid number:=seq_cun.nextval;
begin
select v('app_user'),to_char(sysdate,'dd/mm/yyyy') into v_user,v_date from dual;

execute immediate 'insert into lds_consultant (consultant_id,cst_name,cst_start_date,create_by,create_date) values (:a,:b,:c,:d,:e)'
using v_userid,v_consultant,sysdate,v_user,v_date;

 execute immediate ' create user '||v_username||' identified by '||v_password;
 execute immediate 'insert into application_users values(:x,:y,:z)' using v_userid ,v_username,v_password;

 execute immediate ' grant create session,resource,connect to '||v_username||' with admin option';
end;?

create or replace procedure delete_consultant(v_consultant in varchar2)
 is
 begin
execute immediate ' delete from lds_consultant where cst_name= :a' using v_consultant;
execute immediate ' delete from application_users where user_id =(select consultant_id from lds_consultant where cst_name =:a)' using v_consultant;

 end;?


create or replace procedure get_datas(table_name varchar2) is
type data is ref cursor;
v_data data;
emprec empl%rowtype;
deptrec empl%rowtype;
v_stmt varchar2(1000);
begin
v_stmt:='select * from '||table_name;
open v_data for v_stmt;
loop
fetch v_data into emprec;
exit when v_data%notfound;
dbms_output.put_line(emprec.name);
end loop;
close v_data;
end;?


create or replace procedure insert_users is
     type crr is ref cursor;
    v_cur crr;
    v_name varchar2(25);
    v_id  number;
    begin
     open v_cur for  select consultant_id, substr(cst_name,1,instr(cst_name,' '))  from lds_consultant where consultant_id between 1 and 13;
    loop
    fetch v_cur into v_id,v_name;
    exit when v_cur%notfound;
   execute immediate 'insert into application_users(user_id,username,password) values(:a,:b,:c)' using v_id,v_name,v_name;
   end loop;
   close v_cur;
   end;?


SELECT 'GRANT SELECT ON LDS_ACCOUNT TO '|| USERNAME||';'
  FROM APPLICATION_USERS JOIN LDS_CONSULTANT ON APPLICATION_USERS.USER_ID= LDS_CONSULTANT.CONSULTANT_ID
   WHERE (TO_DATE(SYSDATE)-TO_DATE(CST_START_DATE))/365<1;
        alter table application_user 
  add constraint unq_username unique (username);


SELECT 'GRANT SELECT ON LDS_account TO '|| u.username  ||';'
  FROM application_user u JOIN LDS_CONSULTANT c ON u.CONSULTANT_ID= c.CONSULTANT_ID
   WHERE (TO_DATE(SYSDATE)-TO_DATE(c.CST_start_DATE))/365<1;

create or replace procedure users_create is
      type crr is ref cursor;
     v_cur crr;
     v_name varchar2(25);
     v_id  number;
     begin
      open v_cur for  select consultant_id, substr(cst_name,1,instr(cst_name,' '))  from lds_consultant where consultant_id between 1 and 13;
     loop
     fetch v_cur into v_id,v_name;
    execute immediate 'create user ' ||v_name||' identified by '||v_name;

     exit when v_cur%notfound;
 end loop;
 close v_cur;
 end;
select username from all_users;
begin 
grant_sel ;
end;

create or replace procedure insert_user is
      type crr is ref cursor;
     v_cur crr;
     v_name varchar2(25);
     v_id  number;
     begin
      open v_cur for  select consultant_id, substr(cst_name,1,instr(cst_name,' '))  from lds_consultant where consultant_id between 1 and 13;
     loop
     fetch v_cur into v_id,v_name;
    execute immediate 'insert into application_user values(:a,:b,:c)' using  v_id,v_name,v_name;

     exit when v_cur%notfound;
 end loop;
 close v_cur;
 end;

begin
insert_user;
end;

create or replace procedure get_datas(table_name varchar2) is
type data is ref cursor;
v_data data;
emprec empl%rowtype;
deptrec empl%rowtype;
v_stmt varchar2(1000);
begin
v_stmt:='select * from '||table_name;
open v_data for v_stmt;
loop
fetch v_data into emprec;
exit when v_data%notfound;
dbms_output.put_line(emprec.name);
end loop;
close v_data;
end;

        Based on: .NET (2017)

C# program that uses SqlCeConnection

using System.Data;
using System.Data.SqlServerCe;
using System.Windows.Forms;

namespace WindowsFormsApplication5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            FillData();
        }
        public void dataParsing()
        {
            //checking the filelist array with HRData header
            foreach (header a in heading)
            {
                lineno = a.headerline;

                //checking the condition
                switch (a.headername)
                {
                    case "Params":
                        {
                            int j = lineno + 1;
                            //seperating the values with tabs
                            string[] newline = LinesOfFile[j].Split('\t');
                            string value = newline[0];
                            int add = 0;
                            int b = 1;
                            string[] paramsValue = new string[22];
                            do
                            {
                                // for (int b = 0; b < value.Length; b++)
                                // {
                                //if (newline[b] == "=")
                                foreach (char ab in value)
                                {
                                    if (ab == '=')
                                    {
                                        paramsValue[add] = value.Substring(b, value.Length - b);
                                        // paramsValue[add] = "" + add;
                                    }

                                    b++;
                                }

                                //  }
                                b = 1;
                                add++;
                                j++;
                                newline = LinesOfFile[j].Split('\t');
                                value = newline[0];

                            } while (j < 24);//newline!=null);


                            //paramsData.Add(new Params
                            //inserting data in the params
                            differentParameters.Version = paramsValue[0];
                            differentParameters.Monitor = paramsValue[1];
                            differentParameters.SMode = paramsValue[2];
                            differentParameters.Date = paramsValue[3];
                            differentParameters.StartTime = paramsValue[4];
                            differentParameters.Length = paramsValue[5];
                            differentParameters.Interval = paramsValue[6];
                            differentParameters.Upper1 = paramsValue[7];
                            differentParameters.Lower1 = paramsValue[8];
                            differentParameters.Upper2 = paramsValue[9];
                            differentParameters.Lower2 = paramsValue[10];
                            differentParameters.Upper3 = paramsValue[11];
                            differentParameters.Lower3 = paramsValue[12];
                            differentParameters.Timer1 = paramsValue[13];
                            differentParameters.Timer2 = paramsValue[14];
                            differentParameters.Timer3 = paramsValue[15];
                            differentParameters.ActiveLimit = paramsValue[16];
                            differentParameters.MaxHR = paramsValue[17];
                            differentParameters.RestHR = paramsValue[18];
                            differentParameters.StartDelay = paramsValue[19];
                            differentParameters.VO2max = paramsValue[20];
                            differentParameters.Weight = paramsValue[21];


                            //adding the getter and setter
                            label1.Text = "Date::";
                            label2.Text = "Start Time::";
                            label3.Text = "Interval::";
                               label4.Text = differentParameters.Date;
                              label5.Text = differentParameters.StartTime;
                             label6.Text = differentParameters.Interval;

                            break;
                        }
                    case "HRData":
                        {


                            //using the loop to till the end of array to get values 
                            for (int j = lineno + 1; j < LinesOfFile.Length; j++)
                            {
                                //Spliting chars with tabs
                                string[] newline = LinesOfFile[j].Split('\t');


                                //Switching different versions 
                                switch (differentParameters.Version)
                                {

                                    case "105":
                                        {
                                            hr.Add(new hrdata
                                            {
                                                HeartRate = int.Parse(newline[0]),
                                                Speed = int.Parse(newline[1]),
                                                Cadence = int.Parse(newline[2])
                                            });

                                            break;
                                        }
                                    case "106":
                                        {


                                            //adding the values 
                                            hr.Add(new hrdata
                                            {
                                                HeartRate = int.Parse(newline[0]),
                                                Speed = double.Parse(newline[1]) * 0.1,
                                                Cadence = int.Parse(newline[2]),
                                                Altitude = int.Parse(newline[3]),
                                                Power = int.Parse(newline[4]),
                                                PowerBalancePedalling = int.Parse(newline[5])
                                            });

                                            break;
                                        }
                                    case "107":
                                        {
                                            //adding the values to the parameters
                                            hr.Add(new hrdata
                                            {
                                                HeartRate = int.Parse(newline[0]),
                                                Speed = double.Parse(newline[1]) * 0.1,
                                                Cadence = int.Parse(newline[2]),
                                                Altitude = int.Parse(newline[3]),
                                                Power = int.Parse(newline[4]),
                                                PowerBalancePedalling = int.Parse(newline[5]),
                                                AirPressure = int.Parse(newline[6])
                                            });
                                            break;
                                        }
                                }
                            }
                            break;
                        }

                }
            }
            

            //putting column name is data grid view as per version of parmas
            string[] columnNames = { " HeartRate", " Speed", " Cadence", " Altitude", " Power", "PowerBalancePedalling" };


            foreach (string col in columnNames)
            {
                dt.Columns.Add(col);
            }


            foreach (hrdata hd in hr)
            {



                dt.Rows.Add(hd.HeartRate, hd.Speed, hd.Cadence, hd.Altitude, hd.Power, hd.PowerBalancePedalling);
            }

            //variables initiated for heart rate
            int minHeartRate = 1000;
            int maxHeartRate = 0, sum = 0;
            int count = 0;

            //variables for altitude ,power,speed

            int minAltitude = 1000;
            int maxAltitude = 0;
            int minPower = 1000;
            int maxPower = 0;
            double minSpeed = 1000;
            double maxSpeed = 0;
            //calculating the data to find maximum,minimum

            foreach (hrdata value in hr)
            {
                //for heart rate
                int hrValue = value.HeartRate;
                if (hrValue < minHeartRate)
                {
                    minHeartRate = hrValue;
                }
                else if (hrValue > maxHeartRate)
                {
                    maxHeartRate = hrValue;
                }
                sum += hrValue;
                count++;
                //for altitude 
                int altValue = value.Altitude;
                if (altValue < minAltitude)
                {
                    minAltitude = altValue;
                }
                else if (altValue > maxAltitude)
                {
                    maxAltitude = altValue;
                }

                //for power
                int PowerValue = value.Power;
                if (PowerValue < minPower)
                {
                    minPower = PowerValue;
                }
                else if (altValue > maxPower)
                {
                    maxPower = PowerValue;
                }
                //speed

                double SpeedValue = value.Speed;
                if (SpeedValue < minSpeed)
                {
                    minSpeed = SpeedValue;
                }
                else if (SpeedValue > maxSpeed)
                {
                    maxSpeed = SpeedValue;
                }


            }
            //calculating the stats of heart rate
            label7.Text ="Average Heart Rate::"+ (sum / count).ToString();
            label8.Text ="Maximum Heart Rate::"+maxHeartRate.ToString();
            label9.Text = "Minimum Heart Rate::" + minHeartRate.ToString();
            
            //calculating for altitude ,power ,speed
            label10.Text ="Maximun Power::" + maxPower.ToString()+"Watt";
            label11.Text ="Minimum Power::" +minPower.ToString()+"Watt";

            label12.Text = "Maximum Altitude::" + maxAltitude.ToString()+"Meter";
            label13.Text = "Minimum Altitude::" + minAltitude.ToString()+"Meter";

            label14.Text = "Maximum Speed::" + maxSpeed.ToString()+"Km/Hr";
            label15.Text = "Minimum Speed::" + minSpeed.ToString() + "Km/Hr";


            ParseData.DataSource = dt;
        }
        public void heartrate()
        {
            


        }

        private void ParseData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void ParseData_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void staticGraphToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            graph gr = new graph();
            gr.setHR(hr);
            gr.Show();
        }

        void FillData()
        {
            // 1
            // Open connection
            using (SqlCeConnection c = new SqlCeConnection(
                Properties.Settings.Default.DataConnectionString))
            {
                c.Open();
                // 2
                // Create new DataAdapter
                using (SqlCeDataAdapter a = new SqlCeDataAdapter(
                    "SELECT * FROM Animals", c))
                {
                    // 3
                    // Use DataAdapter to fill DataTable
                    DataTable t = new DataTable();
                    a.Fill(t);
                    // 4
                    // Render data onto the screen
                    dataGridView1.DataSource = t;
                }
            }
        }
    }
}
    }
}

    }using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolarHrmData_Assignment1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        

        //Declaring Array for storing different line data into it
        private string[] LinesOfFile;

        //Creating list and object for different class
        List<header> headingValue = new List<header>();
        public List<hrdata> hr = new List<hrdata>();
        //List<parameters> parameterValue = new List<parameters>();
        parameters allParameters = new parameters();


        int lineNo; //line numbers value


        //datatables for the values

        DataTable dt = new DataTable();
        
        private void Form1_Load(object sender, EventArgs e)
        {
            cycleInfo.Visible = false;
            label7.Visible = false;
            label8.Visible = false;
            label9.Visible = false;
            label10.Visible = false;
            label11.Visible = false;
            label12.Visible = false;
            label13.Visible = false;
            label14.Visible = false;
            label15.Visible = false;
        }

        private void browseFile_Click_1(object sender, EventArgs e)
        {
            using (OpenFileDialog browseFile = new OpenFileDialog())
            {
                if (browseFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string filename = browseFile.FileName;
                    //Reading the File
                    MessageBox.Show("."+filename);

                    LinesOfFile = File.ReadAllLines(filename);
                    
                    //Executing the loop only when the file is not empty.
                    for (int linecount = 0; linecount < LinesOfFile.Length; linecount++)
                    {
                        if (LinesOfFile[linecount].Length > 0)
                        {
                            //Finding The Header and assigning it to Header Class, Here header is between []
                            if (LinesOfFile[linecount][0] == '[' && LinesOfFile[linecount][LinesOfFile[linecount].Length - 1] == ']')
                            {
                                //Assigning Headername to header list
                                headingValue.Add(new header { headername = LinesOfFile[linecount].Substring(1, LinesOfFile[linecount].Length - 2), headerline = linecount });
                                cycleInfo.Visible = true;
                                label7.Visible = true;
                                label8.Visible = true;
                                label9.Visible = true;
                                label10.Visible = true;
                                label11.Visible = true;
                                label12.Visible = true;
                                label13.Visible = true;
                                label14.Visible = true;
                                label15.Visible = true;
                            }
                        }
                    }


                }
                //calling method to store particular set of data in objects

                fileParse();

            }
        }

        public void fileParse()
        {
            //checking the filelist array with HRData header
            foreach (header a in headingValue)
            {
                lineNo = a.headerline;

                //checking the condition
                switch (a.headername)
                {
                    case "Params":
                        {
                            int nextLine = lineNo + 1;
                            //seperating the values with tabs
                            string[] newline = LinesOfFile[nextLine].Split('\t');
                            string value = newline[0];
                            int add = 0;
                            int addval = 1;
                            string[] paramsValue = new string[22];
                            do
                            {
                                
                                foreach (char checkequal in value)
                                {
                                    if (checkequal == '=')
                                    {
                                        paramsValue[add] = value.Substring(addval, value.Length - addval);
                                        
                                    }

                                    addval++;
                                }

                                
                                addval = 1;
                                add++;
                                nextLine++;
                                newline = LinesOfFile[nextLine].Split('\t');
                                value = newline[0];

                            } while (nextLine < 24);//newline!=null);

                           
                            allParameters.Version = paramsValue[0];
                            allParameters.Monitor = paramsValue[1];
                            allParameters.SMode = paramsValue[2];
                            allParameters.Date = paramsValue[3];
                            allParameters.StartTime = paramsValue[4];
                            allParameters.Length = paramsValue[5];
                            allParameters.Interval = paramsValue[6];
                            allParameters.Upper1 = paramsValue[7];
                            allParameters.Lower1 = paramsValue[8];
                            allParameters.Upper2 = paramsValue[9];
                            allParameters.Lower2 = paramsValue[10];
                            allParameters.Upper3 = paramsValue[11];
                            allParameters.Lower3 = paramsValue[12];
                            allParameters.Timer1 = paramsValue[13];
                            allParameters.Timer2 = paramsValue[14];
                            allParameters.Timer3 = paramsValue[15];
                            allParameters.ActiveLimit = paramsValue[16];
                            allParameters.MaxHR = paramsValue[17];
                            allParameters.RestHR = paramsValue[18];
                            allParameters.StartDelay = paramsValue[19];
                            allParameters.VO2max = paramsValue[20];
                            allParameters.Weight = paramsValue[21];


                            //adding the getter and setter
                            label16.Text = "Version: " + allParameters.Version;
                            label17.Text = "Monitor: " + allParameters.Monitor;
                            label18.Text = "SMode: " + allParameters.SMode;
                            label19.Text = "Date: " + allParameters.Date;
                            label20.Text = "Start Time: " +allParameters.StartTime;
                            label21.Text = "Length: " + allParameters.Length;
                            label22.Text = "Interval: " + allParameters.Interval;
                            label23.Text = "Upper1: " + allParameters.Upper1;
                            label24.Text = "Upper2: " + allParameters.Upper2;
                            label25.Text = "Upper3: " + allParameters.Upper3;
                            label26.Text = "Lower1: " + allParameters.Lower1;
                            label27.Text = "Lower2: " + allParameters.Lower2;
                            label28.Text = "Lower3: " + allParameters.Lower3;
                            label29.Text = "Timer1: " + allParameters.Timer1;
                            label31.Text = "Timer2: " + allParameters.Timer2;
                            label32.Text = "Timer3: " + allParameters.Timer3;
                            label33.Text = "ActiveLimit: " + allParameters.ActiveLimit;
                            label34.Text = "MaxHR: " + allParameters.MaxHR;
                            label35.Text = "RestHR: " + allParameters.RestHR;
                            label36.Text = "StartDelay: " + allParameters.StartDelay;
                            label37.Text = "VO2max: " + allParameters.VO2max;
                            label38.Text = "Weight: " + allParameters.Weight;

                            

                            break;
                        }
                    case "HRData":
                        {


                            //using the loop to till the end of array to get values 
                            for (int j = lineNo + 1; j < LinesOfFile.Length; j++)
                            {
                                //Spliting chars with tabs
                                string[] newline = LinesOfFile[j].Split('\t');


                                //Switching different versions 
                                switch (allParameters.Version)
                                {

                                    case "105":
                                        {
                                            hr.Add(new hrdata
                                            {
                                                HeartRate = int.Parse(newline[0]),
                                                Speed = int.Parse(newline[1]),
                                                Cadence = int.Parse(newline[2])
                                            });

                                            break;
                                        }
                                    case "106":
                                        {


                                            //adding the values 
                                            hr.Add(new hrdata
                                            {
                                                HeartRate = int.Parse(newline[0]),
                                                Speed = double.Parse(newline[1]) * 0.1,
                                                Cadence = int.Parse(newline[2]),
                                                Altitude = int.Parse(newline[3]),
                                                Power = int.Parse(newline[4]),
                                                PowerBalancePedalling = int.Parse(newline[5])
                                            });

                                            break;
                                        }
                                    case "107":
                                        {
                                            //adding the values to the parameters
                                            hr.Add(new hrdata
                                            {
                                                HeartRate = int.Parse(newline[0]),
                                                Speed = double.Parse(newline[1]) * 0.1,
                                                Cadence = int.Parse(newline[2]),
                                                Altitude = int.Parse(newline[3]),
                                                Power = int.Parse(newline[4]),
                                                PowerBalancePedalling = int.Parse(newline[5]),
                                                AirPressure = int.Parse(newline[6])
                                            });
                                            break;
                                        }
                                }
                            }
                            break;
                        }

                }
            }
            

            //putting column name is data grid view as per version of parmas
            string[] columnNames = { " HeartRate", " Speed", " Cadence", " Altitude", " Power", "PowerBalancePedalling" };


            foreach (string col in columnNames)
            {
                dt.Columns.Add(col);
            }


            foreach (hrdata hd in hr)
            {



                dt.Rows.Add(hd.HeartRate, hd.Speed, hd.Cadence, hd.Altitude, hd.Power, hd.PowerBalancePedalling);
            }

            //variables initiated for heart rate
            int minHeartRate = 1000;
            int maxHeartRate = 0, sum = 0;
            int count = 0;

            //variables for altitude ,power,speed

            int minAltitude = 1000;
            int maxAltitude = 0;
            int minPower = 1000;
            int maxPower = 0;
            double minSpeed = 1000;
            double maxSpeed = 0;
            //calculating the data to find maximum,minimum

            foreach (hrdata value in hr)
            {
                //for heart rate
                int hrValue = value.HeartRate;
                if (hrValue < minHeartRate)
                {
                    minHeartRate = hrValue;
                }
                else if (hrValue > maxHeartRate)
                {
                    maxHeartRate = hrValue;
                }
                sum += hrValue;
                count++;
                //for altitude 
                int altValue = value.Altitude;
                if (altValue < minAltitude)
                {
                    minAltitude = altValue;
                }
                else if (altValue > maxAltitude)
                {
                    maxAltitude = altValue;
                }

                //for power
                int PowerValue = value.Power;
                if (PowerValue < minPower)
                {
                    minPower = PowerValue;
                }
                else if (altValue > maxPower)
                {
                    maxPower = PowerValue;
                }
                //speed

                double SpeedValue = value.Speed;
                if (SpeedValue < minSpeed)
                {
                    minSpeed = SpeedValue;
                }
                else if (SpeedValue > maxSpeed)
                {
                    maxSpeed = SpeedValue;
                }


            }
            //calculating the stats of heart rate
            label7.Text ="Average Heart Rate::"+ (sum / count).ToString();
            label8.Text ="Maximum Heart Rate::"+maxHeartRate.ToString();
            label9.Text = "Minimum Heart Rate::" + minHeartRate.ToString();
            
            //calculating for altitude ,power ,speed
            label10.Text ="Maximun Power::" + maxPower.ToString()+"Watt";
            label11.Text ="Minimum Power::" +minPower.ToString()+"Watt";

            label12.Text = "Maximum Altitude::" + maxAltitude.ToString()+"Meter";
            label13.Text = "Minimum Altitude::" + minAltitude.ToString()+"Meter";

            label14.Text = "Maximum Speed::" + maxSpeed.ToString()+"Km/Hr";
            label15.Text = "Minimum Speed::" + minSpeed.ToString() + "Km/Hr";


            cycleInfo.DataSource = dt;
        }
        public void heartrate()
        {
            


        }

        private void ParseData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void ParseData_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void staticGraphToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            graph gr = new graph();
            gr.setHR(hr);
            gr.Show();
        }

        private void tabControl1_Click(object sender, EventArgs e)
        {

        }

        //public List<hrdata> getHRDataList()
        //{
        //    return hr;
        //}
   
Google
About 21,000,000 results (0.50 seconds) 
Search Results
The syntax to create a function in Oracle is: CREATE [OR REPLACE] FUNCTION function_name [ (parameter [,parameter]) ] RETURN return_datatype IS. AS [declaration_section] BEGIN executable_section [EXCEPTION exception_section] END [function_name]; When you create a procedure or function, you may define parameters.
Oracle / PLSQL: Functions - TechOnTheNet
https://www.techonthenet.com/oracle/functions.php
Feedback
About this result
People also ask
What is the function in PL SQL?
What is the function of a character?
What is the use of packages in PL SQL?
How do I create a function in Excel?

Feedback
Oracle / PLSQL: Functions - TechOnTheNet
https://www.techonthenet.com/oracle/functions.php

The syntax to create a function in Oracle is: CREATE [OR REPLACE] FUNCTION function_name [ (parameter [,parameter]) ] RETURN return_datatype IS. AS [declaration_section] BEGIN executable_section [EXCEPTION exception_section] END [function_name]; When you create a procedure or function, you may define parameters.
CREATE FUNCTION
https://docs.oracle.com/cd/B19306_01/server.102/b14200/statements_5009.htm

The return value can have any datatype supported by PL/SQL. Note: Oracle SQL does not support calling of functions with Boolean parameters or returns. ... Oracle Database derives the length, precision, or scale of the return value from the environment from which the function is called.
5 Using Procedures, Functions, and Packages - Oracle Help Center
https://docs.oracle.com/cd/B25329_01/doc/appdev.102/.../xedev_programs.htm

Stored procedures and functions (subprograms) can be compiled and stored in an Oracle Database XE, ready to be executed. Once compiled, it is a schema object known as a stored procedure or stored function, which can be referenced or called any number of times by multiple applications connected to Oracle Database ...
‎Overview of Procedures ... · ‎Managing Stored ... · ‎Oracle Provided Packages
You visited this page on 2/5/18.
CREATE FUNCTION Statement
https://docs.oracle.com/cloud/latest/db112/LNPLS/create_function.htm

To embed a CREATE FUNCTION statement inside an Oracle precompiler program, you must terminate the statement with the keyword END-EXEC followed by the embedded SQL statement terminator for the specific language. See Also: For more information about such prerequisites: Oracle Database Advanced Application ...
SQL Functions - Oracle Help Center
https://docs.oracle.com/cd/B19306_01/server.102/b14200/functions001.htm

If you call a SQL function with an argument of a datatype other than the datatype expected by the SQL function, then Oracle attempts to convert the argument to the expected datatype before performing the SQL function. If you call a SQL function with a null argument, then the SQL function automatically returns null. The only ...
Oracle Functions - W3Schools
https://www.w3schools.com/sql/sql_ref_oracle.asp

Function, Description. ASCII, Returns the number code that represents the specified character. ASCIISTR, Converts a string in any character set to an ASCII string using the database character set. CHR, Returns the character based on the number code. COMPOSE, Returns a Unicode string. CONCAT, Allows you to ...
PL/SQL function examples - Burleson Consulting
www.dba-oracle.com/t_plsql_function_examples.htm

PL/SQL function examples. ... Oracle PL/SQL Tips by Donald BurlesonMarch 18, 2015. Question: I want some examples of the PL/SQL functions syntax. Answer: PL/SQL functions allow only INPUT parameters and they must return a value. Here is a PL/SQL function example to change Fahrenheit temperatures to centigrade:.
CREATE FUNCTION - Examples - Oracle Wiki - Oracle - Toad World
https://www.toadworld.com/platforms/oracle/w/wiki/4205.create-function-examples

Mar 13, 2013 - It is suggested to use the CREATE OR REPLACE command to create functions. This allows replacement of existing triggers. To ensure you don't replace a FUNCTION that is already in your schema, query the USER_TRIGGERS view before issuing the CREATE OR REPLACE FUNCTION command.
Oracle PL/SQL – CREATE function example - Mkyong
https://www.mkyong.com/oracle/oracle-plsql-create-function-example/

Oracle PL/SQL – CREATE function example. By Dhaval Dadhaniya | August 30, 2017 | Viewed : 11,504 times +658 pv/w. This article will help you to understand how to create a user defined function. It's also known as stored function or user function. User defined functions are similar to procedures. The only difference is ...
Examples for Creating Oracle Functions - CodeProject
https://www.codeproject.com › Database › Database

Mar 13, 2011 - Even though some of the functions are just short 'aliases' to native Oracle functions, I wanted to create a bit more intuitive versions. Most of the functions are defined as DETERMINISTIC meaning that the function always produces the same result if the input values are the same. So these functions can be ...
Searches related to function in oracle

procedures and functions in oracle with examples

how to call a function in pl/sql

how to call a function in oracle

user defined functions in oracle

oracle function example return varchar2

oracle function example return multiple values

oracle functions pdf

pl sql procedures and functions with example pdf
	1	
2
	
3
	
4
	
5
	
6
	
7
	
8
	
9
	private void data_selection_Load(object sender, EventArgs e)
        {
            GraphPane myPane = zedGraphControl1.GraphPane;

            // Setting the Titles
            myPane.Title.Text = "Graph Analytics for Speed, Cadence, Altitude, Heart Rate and Power";
            myPane.XAxis.Title.Text = "Time in seconds";
            myPane.YAxis.Title.Text = "Individual Units of Measurements";

            int[] heart_rate;
            int[] speed;
            int[] cadence;
            int[] altitude;
            int[] power;

            dataselect.Columns.Add("SNo", "S.No.");
            dataselect.Columns.Add("HR", "Heart");
            dataselect.Columns.Add("SPD", "Speed");
            dataselect.Columns.Add("CAD", "Cadence");
            dataselect.Columns.Add("ALT", "Altitude");
            dataselect.Columns.Add("PWR", "Power");

            try
            {
                string txtData = File.ReadAllText(file_name);
                string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                int index2 = Array.IndexOf(arrData, "[HRData]");
                heart_rate = new int[arrData.Length - (index2 + 1)];
                speed = new int[arrData.Length - (index2 + 1)];
                cadence = new int[arrData.Length - (index2 + 1)];
                altitude = new int[arrData.Length - (index2 + 1)];
                power = new int[arrData.Length - (index2 + 1)];

                int j = 0;
                double total = 0;
                double hr = 0;
                double pwrtotal = 0;
                double alt = 0;
                double sno = 1;


                for (int i = index2 + 1; i < arrData.Length; i++)
                {
                    string HRData = arrData[i];
                    string[] arrHrdata = Regex.Split(HRData, @"\W+");

                    heart_rate[j] = Convert.ToInt32(arrHrdata[0]);
                    speed[j] = Convert.ToInt32(arrHrdata[1]);
                    cadence[j] = Convert.ToInt32(arrHrdata[2]);
                    altitude[j] = Convert.ToInt32(arrHrdata[3]);
                    power[j] = Convert.ToInt32(arrHrdata[4]);

                    // for Average speed
                    double spd = Convert.ToDouble(arrHrdata[1]) / 10;
                    double xa = speed[j];
                    total = total + xa;

                    //Average heart Rate
                    double hrt = heart_rate[j];
                    hr = hrt + hrt;

                    //Average power
                    double pwr = power[j];
                    pwrtotal = pwrtotal + pwr;

                    double altd = altitude[j];
                    alt = alt + altd;

                    // Display data in DataGrid view
                    dataselect.Rows.Add(new object[] { sno++, arrHrdata[0], spd.ToString(), cadence[j], altitude[j], power[j] });
                    j++;
                }


            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataselect_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            int i = 0, count = dataselect.SelectedRows.Count;

            string[] heart_rate = new string[count];
            string[] speed = new string[count];
            string[] cadence = new string[count];
            string[] altitude = new string[count];
            string[] power = new string[count];

            foreach (DataGridViewRow row in dataselect.SelectedRows)
            {
                heart_rate[i] = row.Cells[1].Value.ToString();
                speed[i] = row.Cells[2].Value.ToString();
                cadence[i] = row.Cells[3].Value.ToString();
                altitude[i] = row.Cells[4].Value.ToString();
                power[i] = row.Cells[5].Value.ToString();

                i++;
            }

            PointPairList heartlist = new PointPairList();
            PointPairList speedlist = new PointPairList();
            PointPairList cadencelist = new PointPairList();
            PointPairList altitudelist = new PointPairList();
            PointPairList powerlist = new PointPairList();

            for (int j = 0; j < count; j++)
            {
                heartlist.Add(j, Double.Parse(heart_rate[j]));
                speedlist.Add(j, Double.Parse(speed[j]));
                cadencelist.Add(j, Double.Parse(cadence[j]));
                altitudelist.Add(j, Double.Parse(altitude[j]));
                powerlist.Add(j, Double.Parse(power[j]));

            }

            ZedGraphControl zedGraph = zedGraphControl1;
            GraphPane mypane = zedGraph.GraphPane;

            mypane.CurveList.Clear();

            mypane.XAxis.Title.Text = "Time";
            mypane.XAxis.Scale.Min = 0;
            mypane.XAxis.Scale.Max = count;


            LineItem Curve1 = mypane.AddCurve("Heart rate", heartlist, Color.Red);
            LineItem Curve2 = mypane.AddCurve("Speed", speedlist, Color.Blue);
            LineItem Curve3 = mypane.AddCurve("Cadence", cadencelist, Color.Green);
            LineItem Curve4 = mypane.AddCurve("Altitude", altitudelist, Color.Yellow);
            LineItem Curve5 = mypane.AddCurve("Power", powerlist, Color.Black);


            zedGraph.AxisChange();
            zedGraph.Invalidate();
        }

        private void dataselect_MouseUp(object sender, MouseEventArgs e)
        {

        }
    }
}

10
	
Next
Nepal Patan - From your search history - Use precise location - Learn more
HelpSend feedbackPrivacyTerms

    }

}

using System.Threading.Tasks;
using System.Windows.Forms;

namespace KYC_form
{
    public partial class login : Form
    {
        public login()
        {
            InitializeComponent();
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            if (txtusername.Text == "admin" && txtusername.Text == "admin")
            {
                DashBoard dashboard = new DashBoard();
                dashboard.Show();
            }
            else if (txtusername.Text == "" || txtpassword.Text == "")
            {
                if (txtusername.Text == "")
                {
                    MessageBox.Show("Username is empty");
                    txtusername.Focus();
                }
                else
                {
                    MessageBox.Show("Password is empty");
                    txtpassword.Focus();
                }
            }
            else if (txtusername.Text != "admin" || txtusername.Text == "admin")
            {
                MessageBox.Show("Incorrect Username and Password Combination");
            }
            
        }



        private void check_Load(object sender, EventArgs e)
        {
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
    using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace read_write
{
    public partial class Form1 : Form
    {
        string nameFile;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            //open.ShowDialog();

            if (open.ShowDialog() == DialogResult.OK)
            { 
                StreamReader r = new StreamReader(open.FileName);
                textBox1.Text =  r.ReadToEnd();
                nameFile = open.FileName;
                r.Close();
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
                //Pass the filepath and filename to the StreamWriter Constructor
                StreamWriter sw = new StreamWriter(nameFile);

                //Write a line of text
                sw.WriteLine(textBox1.Text);

                //Close the file
                sw.Close();
           
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace txt_to_dgv
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
                OpenFileDialog open1 = new OpenFileDialog();

                if (open1.ShowDialog() == DialogResult.OK)
                {

                    string fileName = open1.FileName;
                    StreamReader filereader = new StreamReader(open1.FileName);

                    string s = filereader.ReadToEnd();
                }

                DataTable dt = new DataTable();
                dt.Columns.Add("Name");
                dt.Columns.Add("RollNo");
                dt.Columns.Add("Address");
                dt.Columns.Add("Mobile");

                StreamReader fileReader = new StreamReader(open1.FileName);

                while (!fileReader.EndOfStream)
                {
                    string line = fileReader.ReadLine();
                    string[] lineColumn = line.Split();
                    dt.Rows.Add(lineColumn);
                }
                dataGridView1.DataSource = dt;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        create or replace procedure pro_consultant(v_consultant in varchar2,v_username in varchar2,v_password in varchar2)
 is
v_date varchar2(20);
v_user varchar2(20);
v_userid number:=seq_cun.nextval;
begin
select v('app_user'),to_char(sysdate,'dd/mm/yyyy') into v_user,v_date from dual;

execute immediate 'insert into lds_consultant (consultant_id,cst_name,cst_start_date,create_by,create_date) values (:a,:b,:c,:d,:e)'
using v_userid,v_consultant,sysdate,v_user,v_date;

 execute immediate ' create user '||v_username||' identified by '||v_password;
 execute immediate 'insert into application_users values(:x,:y,:z)' using v_userid ,v_username,v_password;

 execute immediate ' grant create session,resource,connect to '||v_username||' with admin option';
end;?

create or replace procedure delete_consultant(v_consultant in varchar2)
 is
 begin
execute immediate ' delete from lds_consultant where cst_name= :a' using v_consultant;
execute immediate ' delete from application_users where user_id =(select consultant_id from lds_consultant where cst_name =:a)' using v_consultant;

 end;?


create or replace procedure get_datas(table_name varchar2) is
type data is ref cursor;
v_data data;
emprec empl%rowtype;
deptrec empl%rowtype;
v_stmt varchar2(1000);
begin
v_stmt:='select * from '||table_name;
open v_data for v_stmt;
loop
fetch v_data into emprec;
exit when v_data%notfound;
dbms_output.put_line(emprec.name);
end loop;
close v_data;
end;?


create or replace procedure insert_users is
     type crr is ref cursor;
    v_cur crr;
    v_name varchar2(25);
    v_id  number;
    begin
     open v_cur for  select consultant_id, substr(cst_name,1,instr(cst_name,' '))  from lds_consultant where consultant_id between 1 and 13;
    loop
    fetch v_cur into v_id,v_name;
    exit when v_cur%notfound;
   execute immediate 'insert into application_users(user_id,username,password) values(:a,:b,:c)' using v_id,v_name,v_name;
   end loop;
   close v_cur;
   end;?


SELECT 'GRANT SELECT ON LDS_ACCOUNT TO '|| USERNAME||';'
  FROM APPLICATION_USERS JOIN LDS_CONSULTANT ON APPLICATION_USERS.USER_ID= LDS_CONSULTANT.CONSULTANT_ID
   WHERE (TO_DATE(SYSDATE)-TO_DATE(CST_START_DATE))/365<1;
        create or replace procedure pro_consultant(v_consultant in varchar2,v_username in varchar2,v_password in varchar2)
 is
v_date varchar2(20);
v_user varchar2(20);
v_userid number:=seq_cun.nextval;
begin
select v('app_user'),to_char(sysdate,'dd/mm/yyyy') into v_user,v_date from dual;

execute immediate 'insert into lds_consultant (consultant_id,cst_name,cst_start_date,create_by,create_date) values (:a,:b,:c,:d,:e)'
using v_userid,v_consultant,sysdate,v_user,v_date;

 execute immediate ' create user '||v_username||' identified by '||v_password;
 execute immediate 'insert into application_users values(:x,:y,:z)' using v_userid ,v_username,v_password;

 execute immediate ' grant create session,resource,connect to '||v_username||' with admin option';
end;?

create or replace procedure delete_consultant(v_consultant in varchar2)
 is
 begin
execute immediate ' delete from lds_consultant where cst_name= :a' using v_consultant;
execute immediate ' delete from application_users where user_id =(select consultant_id from lds_consultant where cst_name =:a)' using v_consultant;

 end;?


create or replace procedure get_datas(table_name varchar2) is
type data is ref cursor;
v_data data;
emprec empl%rowtype;
deptrec empl%rowtype;
v_stmt varchar2(1000);
begin
v_stmt:='select * from '||table_name;
open v_data for v_stmt;
loop
fetch v_data into emprec;
exit when v_data%notfound;
dbms_output.put_line(emprec.name);
end loop;
close v_data;
end;?


create or replace procedure insert_users is
     type crr is ref cursor;
    v_cur crr;
    v_name varchar2(25);
    v_id  number;
    begin
     open v_cur for  select consultant_id, substr(cst_name,1,instr(cst_name,' '))  from lds_consultant where consultant_id between 1 and 13;
    loop
    fetch v_cur into v_id,v_name;
    exit when v_cur%notfound;
   execute immediate 'insert into application_users(user_id,username,password) values(:a,:b,:c)' using v_id,v_name,v_name;
   end loop;
   close v_cur;
   end;?


SELECT 'GRANT SELECT ON LDS_ACCOUNT TO '|| USERNAME||';'
  FROM APPLICATION_USERS JOIN LDS_CONSULTANT ON APPLICATION_USERS.USER_ID= LDS_CONSULTANT.CONSULTANT_ID
   WHERE (TO_DATE(SYSDATE)-TO_DATE(CST_START_DATE))/365<1;
        alter table application_user 
  add constraint unq_username unique (username);


SELECT 'GRANT SELECT ON LDS_account TO '|| u.username  ||';'
  FROM application_user u JOIN LDS_CONSULTANT c ON u.CONSULTANT_ID= c.CONSULTANT_ID
   WHERE (TO_DATE(SYSDATE)-TO_DATE(c.CST_start_DATE))/365<1;

create or replace procedure users_create is
      type crr is ref cursor;
     v_cur crr;
     v_name varchar2(25);
     v_id  number;
     begin
      open v_cur for  select consultant_id, substr(cst_name,1,instr(cst_name,' '))  from lds_consultant where consultant_id between 1 and 13;
     loop
     fetch v_cur into v_id,v_name;
    execute immediate 'create user ' ||v_name||' identified by '||v_name;

     exit when v_cur%notfound;
 end loop;
 close v_cur;
 end;
select username from all_users;
begin 
grant_sel ;
end;

create or replace procedure insert_user is
      type crr is ref cursor;
     v_cur crr;
     v_name varchar2(25);
     v_id  number;
     begin
      open v_cur for  select consultant_id, substr(cst_name,1,instr(cst_name,' '))  from lds_consultant where consultant_id between 1 and 13;
     loop
     fetch v_cur into v_id,v_name;
    execute immediate 'insert into application_user values(:a,:b,:c)' using  v_id,v_name,v_name;

     exit when v_cur%notfound;
 end loop;
 close v_cur;
 end;

begin
insert_user;
end;

create or replace procedure get_datas(table_name varchar2) is
type data is ref cursor;
v_data data;
emprec empl%rowtype;
deptrec empl%rowtype;
v_stmt varchar2(1000);
begin
v_stmt:='select * from '||table_name;
open v_data for v_stmt;
loop
fetch v_data into emprec;
exit when v_data%notfound;
dbms_output.put_line(emprec.name);
end loop;
close v_data;
end;

        Based on: .NET (2017)

C# program that uses SqlCeConnection

using System.Data;
using System.Data.SqlServerCe;
using System.Windows.Forms;

namespace WindowsFormsApplication5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            FillData();
        }
        public void dataParsing()
        {
            //checking the filelist array with HRData header
            foreach (header a in heading)
            {
                lineno = a.headerline;

                //checking the condition
                switch (a.headername)
                {
                    case "Params":
                        {
                            int j = lineno + 1;
                            //seperating the values with tabs
                            string[] newline = LinesOfFile[j].Split('\t');
                            string value = newline[0];
                            int add = 0;
                            int b = 1;
                            string[] paramsValue = new string[22];
                            do
                            {
                                // for (int b = 0; b < value.Length; b++)
                                // {
                                //if (newline[b] == "=")
                                foreach (char ab in value)
                                {
                                    if (ab == '=')
                                    {
                                        paramsValue[add] = value.Substring(b, value.Length - b);
                                        // paramsValue[add] = "" + add;
                                    }

                                    b++;
                                }

                                //  }
                                b = 1;
                                add++;
                                j++;
                                newline = LinesOfFile[j].Split('\t');
                                value = newline[0];

                            } while (j < 24);//newline!=null);


                            //paramsData.Add(new Params
                            //inserting data in the params
                            differentParameters.Version = paramsValue[0];
                            differentParameters.Monitor = paramsValue[1];
                            differentParameters.SMode = paramsValue[2];
                            differentParameters.Date = paramsValue[3];
                            differentParameters.StartTime = paramsValue[4];
                            differentParameters.Length = paramsValue[5];
                            differentParameters.Interval = paramsValue[6];
                            differentParameters.Upper1 = paramsValue[7];
                            differentParameters.Lower1 = paramsValue[8];
                            differentParameters.Upper2 = paramsValue[9];
                            differentParameters.Lower2 = paramsValue[10];
                            differentParameters.Upper3 = paramsValue[11];
                            differentParameters.Lower3 = paramsValue[12];
                            differentParameters.Timer1 = paramsValue[13];
                            differentParameters.Timer2 = paramsValue[14];
                            differentParameters.Timer3 = paramsValue[15];
                            differentParameters.ActiveLimit = paramsValue[16];
                            differentParameters.MaxHR = paramsValue[17];
                            differentParameters.RestHR = paramsValue[18];
                            differentParameters.StartDelay = paramsValue[19];
                            differentParameters.VO2max = paramsValue[20];
                            differentParameters.Weight = paramsValue[21];


                            //adding the getter and setter
                            label1.Text = "Date::";
                            label2.Text = "Start Time::";
                            label3.Text = "Interval::";
                               label4.Text = differentParameters.Date;
                              label5.Text = differentParameters.StartTime;
                             label6.Text = differentParameters.Interval;

                            break;
                        }
                    case "HRData":
                        {


                            //using the loop to till the end of array to get values 
                            for (int j = lineno + 1; j < LinesOfFile.Length; j++)
                            {
                                //Spliting chars with tabs
                                string[] newline = LinesOfFile[j].Split('\t');


                                //Switching different versions 
                                switch (differentParameters.Version)
                                {

                                    case "105":
                                        {
                                            hr.Add(new hrdata
                                            {
                                                HeartRate = int.Parse(newline[0]),
                                                Speed = int.Parse(newline[1]),
                                                Cadence = int.Parse(newline[2])
                                            });

                                            break;
                                        }
                                    case "106":
                                        {


                                            //adding the values 
                                            hr.Add(new hrdata
                                            {
                                                HeartRate = int.Parse(newline[0]),
                                                Speed = double.Parse(newline[1]) * 0.1,
                                                Cadence = int.Parse(newline[2]),
                                                Altitude = int.Parse(newline[3]),
                                                Power = int.Parse(newline[4]),
                                                PowerBalancePedalling = int.Parse(newline[5])
                                            });

                                            break;
                                        }
                                    case "107":
                                        {
                                            //adding the values to the parameters
                                            hr.Add(new hrdata
                                            {
                                                HeartRate = int.Parse(newline[0]),
                                                Speed = double.Parse(newline[1]) * 0.1,
                                                Cadence = int.Parse(newline[2]),
                                                Altitude = int.Parse(newline[3]),
                                                Power = int.Parse(newline[4]),
                                                PowerBalancePedalling = int.Parse(newline[5]),
                                                AirPressure = int.Parse(newline[6])
                                            });
                                            break;
                                        }
                                }
                            }
                            break;
                        }

                }
            }
            

            //putting column name is data grid view as per version of parmas
            string[] columnNames = { " HeartRate", " Speed", " Cadence", " Altitude", " Power", "PowerBalancePedalling" };


            foreach (string col in columnNames)
            {
                dt.Columns.Add(col);
            }


            foreach (hrdata hd in hr)
            {



                dt.Rows.Add(hd.HeartRate, hd.Speed, hd.Cadence, hd.Altitude, hd.Power, hd.PowerBalancePedalling);
            }

            //variables initiated for heart rate
            int minHeartRate = 1000;
            int maxHeartRate = 0, sum = 0;
            int count = 0;

            //variables for altitude ,power,speed

            int minAltitude = 1000;
            int maxAltitude = 0;
            int minPower = 1000;
            int maxPower = 0;
            double minSpeed = 1000;
            double maxSpeed = 0;
            //calculating the data to find maximum,minimum

            foreach (hrdata value in hr)
            {
                //for heart rate
                int hrValue = value.HeartRate;
                if (hrValue < minHeartRate)
                {
                    minHeartRate = hrValue;
                }
                else if (hrValue > maxHeartRate)
                {
                    maxHeartRate = hrValue;
                }
                sum += hrValue;
                count++;
                //for altitude 
                int altValue = value.Altitude;
                if (altValue < minAltitude)
                {
                    minAltitude = altValue;
                }
                else if (altValue > maxAltitude)
                {
                    maxAltitude = altValue;
                }

                //for power
                int PowerValue = value.Power;
                if (PowerValue < minPower)
                {
                    minPower = PowerValue;
                }
                else if (altValue > maxPower)
                {
                    maxPower = PowerValue;
                }
                //speed

                double SpeedValue = value.Speed;
                if (SpeedValue < minSpeed)
                {
                    minSpeed = SpeedValue;
                }
                else if (SpeedValue > maxSpeed)
                {
                    maxSpeed = SpeedValue;
                }


            }
            //calculating the stats of heart rate
            label7.Text ="Average Heart Rate::"+ (sum / count).ToString();
            label8.Text ="Maximum Heart Rate::"+maxHeartRate.ToString();
            label9.Text = "Minimum Heart Rate::" + minHeartRate.ToString();
            
            //calculating for altitude ,power ,speed
            label10.Text ="Maximun Power::" + maxPower.ToString()+"Watt";
            label11.Text ="Minimum Power::" +minPower.ToString()+"Watt";

            label12.Text = "Maximum Altitude::" + maxAltitude.ToString()+"Meter";
            label13.Text = "Minimum Altitude::" + minAltitude.ToString()+"Meter";

            label14.Text = "Maximum Speed::" + maxSpeed.ToString()+"Km/Hr";
            label15.Text = "Minimum Speed::" + minSpeed.ToString() + "Km/Hr";


            ParseData.DataSource = dt;
        }
        public void heartrate()
        {
            


        }

        private void ParseData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void ParseData_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void staticGraphToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            graph gr = new graph();
            gr.setHR(hr);
            gr.Show();
        }

        void FillData()
        {
            // 1
            // Open connection
            using (SqlCeConnection c = new SqlCeConnection(
                Properties.Settings.Default.DataConnectionString))
            {
                c.Open();
                // 2
                // Create new DataAdapter
                using (SqlCeDataAdapter a = new SqlCeDataAdapter(
                    "SELECT * FROM Animals", c))
                {
                    // 3
                    // Use DataAdapter to fill DataTable
                    DataTable t = new DataTable();
                    a.Fill(t);
                    // 4
                    // Render data onto the screen
                    dataGridView1.DataSource = t;
                }
            }
        }
    }
}
    }
}

    }
}


        }
    }
}
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolarHrmData_Assignment1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        

        //Declaring Array for storing different line data into it
        private string[] LinesOfFile;

        //Creating list and object for different class
        List<header> headingValue = new List<header>();
        public List<hrdata> hr = new List<hrdata>();
        //List<parameters> parameterValue = new List<parameters>();
        parameters allParameters = new parameters();


        int lineNo; //line numbers value


        //datatables for the values

        DataTable dt = new DataTable();
        
        private void Form1_Load(object sender, EventArgs e)
        {
            cycleInfo.Visible = false;
            label7.Visible = false;
            label8.Visible = false;
            label9.Visible = false;
            label10.Visible = false;
            label11.Visible = false;
            label12.Visible = false;
            label13.Visible = false;
            label14.Visible = false;
            label15.Visible = false;
        }

        private void browseFile_Click_1(object sender, EventArgs e)
        {
            using (OpenFileDialog browseFile = new OpenFileDialog())
            {
                if (browseFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string filename = browseFile.FileName;
                    //Reading the File
                    MessageBox.Show("."+filename);

                    LinesOfFile = File.ReadAllLines(filename);
                    
                    //Executing the loop only when the file is not empty.
                    for (int linecount = 0; linecount < LinesOfFile.Length; linecount++)
                    {
                        if (LinesOfFile[linecount].Length > 0)
                        {
                            //Finding The Header and assigning it to Header Class, Here header is between []
                            if (LinesOfFile[linecount][0] == '[' && LinesOfFile[linecount][LinesOfFile[linecount].Length - 1] == ']')
                            {
                                //Assigning Headername to header list
                                headingValue.Add(new header { headername = LinesOfFile[linecount].Substring(1, LinesOfFile[linecount].Length - 2), headerline = linecount });
                                cycleInfo.Visible = true;
                                label7.Visible = true;
                                label8.Visible = true;
                                label9.Visible = true;
                                label10.Visible = true;
                                label11.Visible = true;
                                label12.Visible = true;
                                label13.Visible = true;
                                label14.Visible = true;
                                label15.Visible = true;
                            }
                        }
                    }


                }
                //calling method to store particular set of data in objects

                fileParse();

            }
        }

        public void fileParse()
        {
            //checking the filelist array with HRData header
            foreach (header a in headingValue)
            {
                lineNo = a.headerline;

                //checking the condition
                switch (a.headername)
                {
                    case "Params":
                        {
                            int nextLine = lineNo + 1;
                            //seperating the values with tabs
                            string[] newline = LinesOfFile[nextLine].Split('\t');
                            string value = newline[0];
                            int add = 0;
                            int addval = 1;
                            string[] paramsValue = new string[22];
                            do
                            {
                                
                                foreach (char checkequal in value)
                                {
                                    if (checkequal == '=')
                                    {
                                        paramsValue[add] = value.Substring(addval, value.Length - addval);
                                        
                                    }

                                    addval++;
                                }

                                
                                addval = 1;
                                add++;
                                nextLine++;
                                newline = LinesOfFile[nextLine].Split('\t');
                                value = newline[0];

                            } while (nextLine < 24);//newline!=null);

                           
                            allParameters.Version = paramsValue[0];
                            allParameters.Monitor = paramsValue[1];
                            allParameters.SMode = paramsValue[2];
                            allParameters.Date = paramsValue[3];
                            allParameters.StartTime = paramsValue[4];
                            allParameters.Length = paramsValue[5];
                            allParameters.Interval = paramsValue[6];
                            allParameters.Upper1 = paramsValue[7];
                            allParameters.Lower1 = paramsValue[8];
                            allParameters.Upper2 = paramsValue[9];
                            allParameters.Lower2 = paramsValue[10];
                            allParameters.Upper3 = paramsValue[11];
                            allParameters.Lower3 = paramsValue[12];
                            allParameters.Timer1 = paramsValue[13];
                            allParameters.Timer2 = paramsValue[14];
                            allParameters.Timer3 = paramsValue[15];
                            allParameters.ActiveLimit = paramsValue[16];
                            allParameters.MaxHR = paramsValue[17];
                            allParameters.RestHR = paramsValue[18];
                            allParameters.StartDelay = paramsValue[19];
                            allParameters.VO2max = paramsValue[20];
                            allParameters.Weight = paramsValue[21];


                            //adding the getter and setter
                            label16.Text = "Version: " + allParameters.Version;
                            label17.Text = "Monitor: " + allParameters.Monitor;
                            label18.Text = "SMode: " + allParameters.SMode;
                            label19.Text = "Date: " + allParameters.Date;
                            label20.Text = "Start Time: " +allParameters.StartTime;
                            label21.Text = "Length: " + allParameters.Length;
                            label22.Text = "Interval: " + allParameters.Interval;
                            label23.Text = "Upper1: " + allParameters.Upper1;
                            label24.Text = "Upper2: " + allParameters.Upper2;
                            label25.Text = "Upper3: " + allParameters.Upper3;
                            label26.Text = "Lower1: " + allParameters.Lower1;
                            label27.Text = "Lower2: " + allParameters.Lower2;
                            label28.Text = "Lower3: " + allParameters.Lower3;
                            label29.Text = "Timer1: " + allParameters.Timer1;
                            label31.Text = "Timer2: " + allParameters.Timer2;
                            label32.Text = "Timer3: " + allParameters.Timer3;
                            label33.Text = "ActiveLimit: " + allParameters.ActiveLimit;
                            label34.Text = "MaxHR: " + allParameters.MaxHR;
                            label35.Text = "RestHR: " + allParameters.RestHR;
                            label36.Text = "StartDelay: " + allParameters.StartDelay;
                            label37.Text = "VO2max: " + allParameters.VO2max;
                            label38.Text = "Weight: " + allParameters.Weight;

                            

                            break;
                        }
                    case "HRData":
                        {


                            //using the loop to till the end of array to get values 
                            for (int j = lineNo + 1; j < LinesOfFile.Length; j++)
                            {
                                //Spliting chars with tabs
                                string[] newline = LinesOfFile[j].Split('\t');


                                //Switching different versions 
                                switch (allParameters.Version)
                                {

                                    case "105":
                                        {
                                            hr.Add(new hrdata
                                            {
                                                HeartRate = int.Parse(newline[0]),
                                                Speed = int.Parse(newline[1]),
                                                Cadence = int.Parse(newline[2])
                                            });

                                            break;
                                        }
                                    case "106":
                                        {


                                            //adding the values 
                                            hr.Add(new hrdata
                                            {
                                                HeartRate = int.Parse(newline[0]),
                                                Speed = double.Parse(newline[1]) * 0.1,
                                                Cadence = int.Parse(newline[2]),
                                                Altitude = int.Parse(newline[3]),
                                                Power = int.Parse(newline[4]),
                                                PowerBalancePedalling = int.Parse(newline[5])
                                            });

                                            break;
                                        }
                                    case "107":
                                        {
                                            //adding the values to the parameters
                                            hr.Add(new hrdata
                                            {
                                                HeartRate = int.Parse(newline[0]),
                                                Speed = double.Parse(newline[1]) * 0.1,
                                                Cadence = int.Parse(newline[2]),
                                                Altitude = int.Parse(newline[3]),
                                                Power = int.Parse(newline[4]),
                                                PowerBalancePedalling = int.Parse(newline[5]),
                                                AirPressure = int.Parse(newline[6])
                                            });
                                            break;
                                        }
                                }
                            }
                            break;
                        }

                }
            }
            

            //putting column name is data grid view as per version of parmas
            string[] columnNames = { " HeartRate", " Speed", " Cadence", " Altitude", " Power", "PowerBalancePedalling" };


            foreach (string col in columnNames)
            {
                dt.Columns.Add(col);
            }


            foreach (hrdata hd in hr)
            {



                dt.Rows.Add(hd.HeartRate, hd.Speed, hd.Cadence, hd.Altitude, hd.Power, hd.PowerBalancePedalling);
            }

            //variables initiated for heart rate
            int minHeartRate = 1000;
            int maxHeartRate = 0, sum = 0;
            int count = 0;

            //variables for altitude ,power,speed

            int minAltitude = 1000;
            int maxAltitude = 0;
            int minPower = 1000;
            int maxPower = 0;
            double minSpeed = 1000;
            double maxSpeed = 0;
            //calculating the data to find maximum,minimum

            foreach (hrdata value in hr)
            {
                //for heart rate
                int hrValue = value.HeartRate;
                if (hrValue < minHeartRate)
                {
                    minHeartRate = hrValue;
                }
                else if (hrValue > maxHeartRate)
                {
                    maxHeartRate = hrValue;
                }
                sum += hrValue;
                count++;
                //for altitude 
                int altValue = value.Altitude;
                if (altValue < minAltitude)
                {
                    minAltitude = altValue;
                }
                else if (altValue > maxAltitude)
                {
                    maxAltitude = altValue;
                }

                //for power
                int PowerValue = value.Power;
                if (PowerValue < minPower)
                {
                    minPower = PowerValue;
                }
                else if (altValue > maxPower)
                {
                    maxPower = PowerValue;
                }
                //speed

                double SpeedValue = value.Speed;
                if (SpeedValue < minSpeed)
                {
                    minSpeed = SpeedValue;
                }
                else if (SpeedValue > maxSpeed)
                {
                    maxSpeed = SpeedValue;
                }


            }
            //calculating the stats of heart rate
            label7.Text ="Average Heart Rate::"+ (sum / count).ToString();
            label8.Text ="Maximum Heart Rate::"+maxHeartRate.ToString();
            label9.Text = "Minimum Heart Rate::" + minHeartRate.ToString();
            
            //calculating for altitude ,power ,speed
            label10.Text ="Maximun Power::" + maxPower.ToString()+"Watt";
            label11.Text ="Minimum Power::" +minPower.ToString()+"Watt";

            label12.Text = "Maximum Altitude::" + maxAltitude.ToString()+"Meter";
            label13.Text = "Minimum Altitude::" + minAltitude.ToString()+"Meter";

            label14.Text = "Maximum Speed::" + maxSpeed.ToString()+"Km/Hr";
            label15.Text = "Minimum Speed::" + minSpeed.ToString() + "Km/Hr";


            cycleInfo.DataSource = dt;
        }
        public void heartrate()
        {
            


        }

        private void ParseData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void ParseData_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void staticGraphToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            graph gr = new graph();
            gr.setHR(hr);
            gr.Show();
        }

        private void tabControl1_Click(object sender, EventArgs e)
        {

        }

        //public List<hrdata> getHRDataList()
        //{
        //    return hr;
        //}
   
Google
About 21,000,000 results (0.50 seconds) 
Search Results
The syntax to create a function in Oracle is: CREATE [OR REPLACE] FUNCTION function_name [ (parameter [,parameter]) ] RETURN return_datatype IS. AS [declaration_section] BEGIN executable_section [EXCEPTION exception_section] END [function_name]; When you create a procedure or function, you may define parameters.
Oracle / PLSQL: Functions - TechOnTheNet
https://www.techonthenet.com/oracle/functions.php
Feedback
About this result
People also ask
What is the function in PL SQL?
What is the function of a character?
What is the use of packages in PL SQL?
How do I create a function in Excel?

Feedback
Oracle / PLSQL: Functions - TechOnTheNet
https://www.techonthenet.com/oracle/functions.php

The syntax to create a function in Oracle is: CREATE [OR REPLACE] FUNCTION function_name [ (parameter [,parameter]) ] RETURN return_datatype IS. AS [declaration_section] BEGIN executable_section [EXCEPTION exception_section] END [function_name]; When you create a procedure or function, you may define parameters.
CREATE FUNCTION
https://docs.oracle.com/cd/B19306_01/server.102/b14200/statements_5009.htm

The return value can have any datatype supported by PL/SQL. Note: Oracle SQL does not support calling of functions with Boolean parameters or returns. ... Oracle Database derives the length, precision, or scale of the return value from the environment from which the function is called.
5 Using Procedures, Functions, and Packages - Oracle Help Center
https://docs.oracle.com/cd/B25329_01/doc/appdev.102/.../xedev_programs.htm

Stored procedures and functions (subprograms) can be compiled and stored in an Oracle Database XE, ready to be executed. Once compiled, it is a schema object known as a stored procedure or stored function, which can be referenced or called any number of times by multiple applications connected to Oracle Database ...
‎Overview of Procedures ... · ‎Managing Stored ... · ‎Oracle Provided Packages
You visited this page on 2/5/18.
CREATE FUNCTION Statement
https://docs.oracle.com/cloud/latest/db112/LNPLS/create_function.htm

To embed a CREATE FUNCTION statement inside an Oracle precompiler program, you must terminate the statement with the keyword END-EXEC followed by the embedded SQL statement terminator for the specific language. See Also: For more information about such prerequisites: Oracle Database Advanced Application ...
SQL Functions - Oracle Help Center
https://docs.oracle.com/cd/B19306_01/server.102/b14200/functions001.htm

If you call a SQL function with an argument of a datatype other than the datatype expected by the SQL function, then Oracle attempts to convert the argument to the expected datatype before performing the SQL function. If you call a SQL function with a null argument, then the SQL function automatically returns null. The only ...
Oracle Functions - W3Schools
https://www.w3schools.com/sql/sql_ref_oracle.asp

Function, Description. ASCII, Returns the number code that represents the specified character. ASCIISTR, Converts a string in any character set to an ASCII string using the database character set. CHR, Returns the character based on the number code. COMPOSE, Returns a Unicode string. CONCAT, Allows you to ...
PL/SQL function examples - Burleson Consulting
www.dba-oracle.com/t_plsql_function_examples.htm

PL/SQL function examples. ... Oracle PL/SQL Tips by Donald BurlesonMarch 18, 2015. Question: I want some examples of the PL/SQL functions syntax. Answer: PL/SQL functions allow only INPUT parameters and they must return a value. Here is a PL/SQL function example to change Fahrenheit temperatures to centigrade:.
CREATE FUNCTION - Examples - Oracle Wiki - Oracle - Toad World
https://www.toadworld.com/platforms/oracle/w/wiki/4205.create-function-examples

Mar 13, 2013 - It is suggested to use the CREATE OR REPLACE command to create functions. This allows replacement of existing triggers. To ensure you don't replace a FUNCTION that is already in your schema, query the USER_TRIGGERS view before issuing the CREATE OR REPLACE FUNCTION command.
Oracle PL/SQL – CREATE function example - Mkyong
https://www.mkyong.com/oracle/oracle-plsql-create-function-example/

Oracle PL/SQL – CREATE function example. By Dhaval Dadhaniya | August 30, 2017 | Viewed : 11,504 times +658 pv/w. This article will help you to understand how to create a user defined function. It's also known as stored function or user function. User defined functions are similar to procedures. The only difference is ...
Examples for Creating Oracle Functions - CodeProject
https://www.codeproject.com › Database › Database

Mar 13, 2011 - Even though some of the functions are just short 'aliases' to native Oracle functions, I wanted to create a bit more intuitive versions. Most of the functions are defined as DETERMINISTIC meaning that the function always produces the same result if the input values are the same. So these functions can be ...
Searches related to function in oracle

procedures and functions in oracle with examples

how to call a function in pl/sql

how to call a function in oracle

user defined functions in oracle

oracle function example return varchar2

oracle function example return multiple values

oracle functions pdf

pl sql procedures and functions with example pdf
	1	
2
	
3
	
4
	
5
	
6
	
7
	
8
	
9
	private void data_selection_Load(object sender, EventArgs e)
        {
            GraphPane myPane = zedGraphControl1.GraphPane;

            // Setting the Titles
            myPane.Title.Text = "Graph Analytics for Speed, Cadence, Altitude, Heart Rate and Power";
            myPane.XAxis.Title.Text = "Time in seconds";
            myPane.YAxis.Title.Text = "Individual Units of Measurements";

            int[] heart_rate;
            int[] speed;
            int[] cadence;
            int[] altitude;
            int[] power;

            dataselect.Columns.Add("SNo", "S.No.");
            dataselect.Columns.Add("HR", "Heart");
            dataselect.Columns.Add("SPD", "Speed");
            dataselect.Columns.Add("CAD", "Cadence");
            dataselect.Columns.Add("ALT", "Altitude");
            dataselect.Columns.Add("PWR", "Power");

            try
            {
                string txtData = File.ReadAllText(file_name);
                string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                int index2 = Array.IndexOf(arrData, "[HRData]");
                heart_rate = new int[arrData.Length - (index2 + 1)];
                speed = new int[arrData.Length - (index2 + 1)];
                cadence = new int[arrData.Length - (index2 + 1)];
                altitude = new int[arrData.Length - (index2 + 1)];
                power = new int[arrData.Length - (index2 + 1)];

                int j = 0;
                double total = 0;
                double hr = 0;
                double pwrtotal = 0;
                double alt = 0;
                double sno = 1;


                for (int i = index2 + 1; i < arrData.Length; i++)
                {
                    string HRData = arrData[i];
                    string[] arrHrdata = Regex.Split(HRData, @"\W+");

                    heart_rate[j] = Convert.ToInt32(arrHrdata[0]);
                    speed[j] = Convert.ToInt32(arrHrdata[1]);
                    cadence[j] = Convert.ToInt32(arrHrdata[2]);
                    altitude[j] = Convert.ToInt32(arrHrdata[3]);
                    power[j] = Convert.ToInt32(arrHrdata[4]);

                    // for Average speed
                    double spd = Convert.ToDouble(arrHrdata[1]) / 10;
                    double xa = speed[j];
                    total = total + xa;

                    //Average heart Rate
                    double hrt = heart_rate[j];
                    hr = hrt + hrt;

                    //Average power
                    double pwr = power[j];
                    pwrtotal = pwrtotal + pwr;

                    double altd = altitude[j];
                    alt = alt + altd;

                    // Display data in DataGrid view
                    dataselect.Rows.Add(new object[] { sno++, arrHrdata[0], spd.ToString(), cadence[j], altitude[j], power[j] });
                    j++;
                }


            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataselect_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            int i = 0, count = dataselect.SelectedRows.Count;

            string[] heart_rate = new string[count];
            string[] speed = new string[count];
            string[] cadence = new string[count];
            string[] altitude = new string[count];
            string[] power = new string[count];

            foreach (DataGridViewRow row in dataselect.SelectedRows)
            {
                heart_rate[i] = row.Cells[1].Value.ToString();
                speed[i] = row.Cells[2].Value.ToString();
                cadence[i] = row.Cells[3].Value.ToString();
                altitude[i] = row.Cells[4].Value.ToString();
                power[i] = row.Cells[5].Value.ToString();

                i++;
            }

            PointPairList heartlist = new PointPairList();
            PointPairList speedlist = new PointPairList();
            PointPairList cadencelist = new PointPairList();
            PointPairList altitudelist = new PointPairList();
            PointPairList powerlist = new PointPairList();

            for (int j = 0; j < count; j++)
            {
                heartlist.Add(j, Double.Parse(heart_rate[j]));
                speedlist.Add(j, Double.Parse(speed[j]));
                cadencelist.Add(j, Double.Parse(cadence[j]));
                altitudelist.Add(j, Double.Parse(altitude[j]));
                powerlist.Add(j, Double.Parse(power[j]));

            }

            ZedGraphControl zedGraph = zedGraphControl1;
            GraphPane mypane = zedGraph.GraphPane;

            mypane.CurveList.Clear();

            mypane.XAxis.Title.Text = "Time";
            mypane.XAxis.Scale.Min = 0;
            mypane.XAxis.Scale.Max = count;


            LineItem Curve1 = mypane.AddCurve("Heart rate", heartlist, Color.Red);
            LineItem Curve2 = mypane.AddCurve("Speed", speedlist, Color.Blue);
            LineItem Curve3 = mypane.AddCurve("Cadence", cadencelist, Color.Green);
            LineItem Curve4 = mypane.AddCurve("Altitude", altitudelist, Color.Yellow);
            LineItem Curve5 = mypane.AddCurve("Power", powerlist, Color.Black);


            zedGraph.AxisChange();
            zedGraph.Invalidate();
        }

        private void dataselect_MouseUp(object sender, MouseEventArgs e)
        {

        }
    }
}

10
	
Next
Nepal Patan - From your search history - Use precise location - Learn more
HelpSend feedbackPrivacyTerms

    }

}

using System.Threading.Tasks;
using System.Windows.Forms;

namespace KYC_form
{
    public partial class login : Form
    {
        public login()
        {
            InitializeComponent();
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            if (txtusername.Text == "admin" && txtusername.Text == "admin")
            {
                DashBoard dashboard = new DashBoard();
                dashboard.Show();
            }
            else if (txtusername.Text == "" || txtpassword.Text == "")
            {
                if (txtusername.Text == "")
                {
                    MessageBox.Show("Username is empty");
                    txtusername.Focus();
                }
                else
                {
                    MessageBox.Show("Password is empty");
                    txtpassword.Focus();
                }
            }
            else if (txtusername.Text != "admin" || txtusername.Text == "admin")
            {
                MessageBox.Show("Incorrect Username and Password Combination");
            }
            
        }



        private void check_Load(object sender, EventArgs e)
        {
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
    using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace read_write
{
    public partial class Form1 : Form
    {
        string nameFile;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            //open.ShowDialog();

            if (open.ShowDialog() == DialogResult.OK)
            { 
                StreamReader r = new StreamReader(open.FileName);
                textBox1.Text =  r.ReadToEnd();
                nameFile = open.FileName;
                r.Close();
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
                //Pass the filepath and filename to the StreamWriter Constructor
                StreamWriter sw = new StreamWriter(nameFile);

                //Write a line of text
                sw.WriteLine(textBox1.Text);

                //Close the file
                sw.Close();
           
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace txt_to_dgv
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
                OpenFileDialog open1 = new OpenFileDialog();

                if (open1.ShowDialog() == DialogResult.OK)
                {

                    string fileName = open1.FileName;
                    StreamReader filereader = new StreamReader(open1.FileName);

                    string s = filereader.ReadToEnd();
                }

                DataTable dt = new DataTable();
                dt.Columns.Add("Name");
                dt.Columns.Add("RollNo");
                dt.Columns.Add("Address");
                dt.Columns.Add("Mobile");

                StreamReader fileReader = new StreamReader(open1.FileName);

                while (!fileReader.EndOfStream)
                {
                    string line = fileReader.ReadLine();
                    string[] lineColumn = line.Split();
                    dt.Rows.Add(lineColumn);
                }
                dataGridView1.DataSource = dt;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        create or replace procedure pro_consultant(v_consultant in varchar2,v_username in varchar2,v_password in varchar2)
 is
v_date varchar2(20);
v_user varchar2(20);
v_userid number:=seq_cun.nextval;
begin
select v('app_user'),to_char(sysdate,'dd/mm/yyyy') into v_user,v_date from dual;

execute immediate 'insert into lds_consultant (consultant_id,cst_name,cst_start_date,create_by,create_date) values (:a,:b,:c,:d,:e)'
using v_userid,v_consultant,sysdate,v_user,v_date;

 execute immediate ' create user '||v_username||' identified by '||v_password;
 execute immediate 'insert into application_users values(:x,:y,:z)' using v_userid ,v_username,v_password;

 execute immediate ' grant create session,resource,connect to '||v_username||' with admin option';
end;?

create or replace procedure delete_consultant(v_consultant in varchar2)
 is
 begin
execute immediate ' delete from lds_consultant where cst_name= :a' using v_consultant;
execute immediate ' delete from application_users where user_id =(select consultant_id from lds_consultant where cst_name =:a)' using v_consultant;

 end;?


create or replace procedure get_datas(table_name varchar2) is
type data is ref cursor;
v_data data;
emprec empl%rowtype;
deptrec empl%rowtype;
v_stmt varchar2(1000);
begin
v_stmt:='select * from '||table_name;
open v_data for v_stmt;
loop
fetch v_data into emprec;
exit when v_data%notfound;
dbms_output.put_line(emprec.name);
end loop;
close v_data;
end;?


create or replace procedure insert_users is
     type crr is ref cursor;
    v_cur crr;
    v_name varchar2(25);
    v_id  number;
    begin
     open v_cur for  select consultant_id, substr(cst_name,1,instr(cst_name,' '))  from lds_consultant where consultant_id between 1 and 13;
    loop
    fetch v_cur into v_id,v_name;
    exit when v_cur%notfound;
   execute immediate 'insert into application_users(user_id,username,password) values(:a,:b,:c)' using v_id,v_name,v_name;
   end loop;
   close v_cur;
   end;?


SELECT 'GRANT SELECT ON LDS_ACCOUNT TO '|| USERNAME||';'
  FROM APPLICATION_USERS JOIN LDS_CONSULTANT ON APPLICATION_USERS.USER_ID= LDS_CONSULTANT.CONSULTANT_ID
   WHERE (TO_DATE(SYSDATE)-TO_DATE(CST_START_DATE))/365<1;
        create or replace procedure pro_consultant(v_consultant in varchar2,v_username in varchar2,v_password in varchar2)
 is
v_date varchar2(20);
v_user varchar2(20);
v_userid number:=seq_cun.nextval;
begin
select v('app_user'),to_char(sysdate,'dd/mm/yyyy') into v_user,v_date from dual;

execute immediate 'insert into lds_consultant (consultant_id,cst_name,cst_start_date,create_by,create_date) values (:a,:b,:c,:d,:e)'
using v_userid,v_consultant,sysdate,v_user,v_date;

 execute immediate ' create user '||v_username||' identified by '||v_password;
 execute immediate 'insert into application_users values(:x,:y,:z)' using v_userid ,v_username,v_password;

 execute immediate ' grant create session,resource,connect to '||v_username||' with admin option';
end;?

create or replace procedure delete_consultant(v_consultant in varchar2)
 is
 begin
execute immediate ' delete from lds_consultant where cst_name= :a' using v_consultant;
execute immediate ' delete from application_users where user_id =(select consultant_id from lds_consultant where cst_name =:a)' using v_consultant;

 end;?


create or replace procedure get_datas(table_name varchar2) is
type data is ref cursor;
v_data data;
emprec empl%rowtype;
deptrec empl%rowtype;
v_stmt varchar2(1000);
begin
v_stmt:='select * from '||table_name;
open v_data for v_stmt;
loop
fetch v_data into emprec;
exit when v_data%notfound;
dbms_output.put_line(emprec.name);
end loop;
close v_data;
end;?


create or replace procedure insert_users is
     type crr is ref cursor;
    v_cur crr;
    v_name varchar2(25);
    v_id  number;
    begin
     open v_cur for  select consultant_id, substr(cst_name,1,instr(cst_name,' '))  from lds_consultant where consultant_id between 1 and 13;
    loop
    fetch v_cur into v_id,v_name;
    exit when v_cur%notfound;
   execute immediate 'insert into application_users(user_id,username,password) values(:a,:b,:c)' using v_id,v_name,v_name;
   end loop;
   close v_cur;
   end;?


SELECT 'GRANT SELECT ON LDS_ACCOUNT TO '|| USERNAME||';'
  FROM APPLICATION_USERS JOIN LDS_CONSULTANT ON APPLICATION_USERS.USER_ID= LDS_CONSULTANT.CONSULTANT_ID
   WHERE (TO_DATE(SYSDATE)-TO_DATE(CST_START_DATE))/365<1;
        alter table application_user 
  add constraint unq_username unique (username);


SELECT 'GRANT SELECT ON LDS_account TO '|| u.username  ||';'
  FROM application_user u JOIN LDS_CONSULTANT c ON u.CONSULTANT_ID= c.CONSULTANT_ID
   WHERE (TO_DATE(SYSDATE)-TO_DATE(c.CST_start_DATE))/365<1;

create or replace procedure users_create is
      type crr is ref cursor;
     v_cur crr;
     v_name varchar2(25);
     v_id  number;
     begin
      open v_cur for  select consultant_id, substr(cst_name,1,instr(cst_name,' '))  from lds_consultant where consultant_id between 1 and 13;
     loop
     fetch v_cur into v_id,v_name;
    execute immediate 'create user ' ||v_name||' identified by '||v_name;

     exit when v_cur%notfound;
 end loop;
 close v_cur;
 end;
select username from all_users;
begin 
grant_sel ;
end;

create or replace procedure insert_user is
      type crr is ref cursor;
     v_cur crr;
     v_name varchar2(25);
     v_id  number;
     begin
      open v_cur for  select consultant_id, substr(cst_name,1,instr(cst_name,' '))  from lds_consultant where consultant_id between 1 and 13;
     loop
     fetch v_cur into v_id,v_name;
    execute immediate 'insert into application_user values(:a,:b,:c)' using  v_id,v_name,v_name;

     exit when v_cur%notfound;
 end loop;
 close v_cur;
 end;

begin
insert_user;
end;

create or replace procedure get_datas(table_name varchar2) is
type data is ref cursor;
v_data data;
emprec empl%rowtype;
deptrec empl%rowtype;
v_stmt varchar2(1000);
begin
v_stmt:='select * from '||table_name;
open v_data for v_stmt;
loop
fetch v_data into emprec;
exit when v_data%notfound;
dbms_output.put_line(emprec.name);
end loop;
close v_data;
end;

        Based on: .NET (2017)

C# program that uses SqlCeConnection

using System.Data;
using System.Data.SqlServerCe;
using System.Windows.Forms;

namespace WindowsFormsApplication5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            FillData();
        }
        public void dataParsing()
        {
            //checking the filelist array with HRData header
            foreach (header a in heading)
            {
                lineno = a.headerline;

                //checking the condition
                switch (a.headername)
                {
                    case "Params":
                        {
                            int j = lineno + 1;
                            //seperating the values with tabs
                            string[] newline = LinesOfFile[j].Split('\t');
                            string value = newline[0];
                            int add = 0;
                            int b = 1;
                            string[] paramsValue = new string[22];
                            do
                            {
                                // for (int b = 0; b < value.Length; b++)
                                // {
                                //if (newline[b] == "=")
                                foreach (char ab in value)
                                {
                                    if (ab == '=')
                                    {
                                        paramsValue[add] = value.Substring(b, value.Length - b);
                                        // paramsValue[add] = "" + add;
                                    }

                                    b++;
                                }

                                //  }
                                b = 1;
                                add++;
                                j++;
                                newline = LinesOfFile[j].Split('\t');
                                value = newline[0];

                            } while (j < 24);//newline!=null);


                            //paramsData.Add(new Params
                            //inserting data in the params
                            differentParameters.Version = paramsValue[0];
                            differentParameters.Monitor = paramsValue[1];
                            differentParameters.SMode = paramsValue[2];
                            differentParameters.Date = paramsValue[3];
                            differentParameters.StartTime = paramsValue[4];
                            differentParameters.Length = paramsValue[5];
                            differentParameters.Interval = paramsValue[6];
                            differentParameters.Upper1 = paramsValue[7];
                            differentParameters.Lower1 = paramsValue[8];
                            differentParameters.Upper2 = paramsValue[9];
                            differentParameters.Lower2 = paramsValue[10];
                            differentParameters.Upper3 = paramsValue[11];
                            differentParameters.Lower3 = paramsValue[12];
                            differentParameters.Timer1 = paramsValue[13];
                            differentParameters.Timer2 = paramsValue[14];
                            differentParameters.Timer3 = paramsValue[15];
                            differentParameters.ActiveLimit = paramsValue[16];
                            differentParameters.MaxHR = paramsValue[17];
                            differentParameters.RestHR = paramsValue[18];
                            differentParameters.StartDelay = paramsValue[19];
                            differentParameters.VO2max = paramsValue[20];
                            differentParameters.Weight = paramsValue[21];


                            //adding the getter and setter
                            label1.Text = "Date::";
                            label2.Text = "Start Time::";
                            label3.Text = "Interval::";
                               label4.Text = differentParameters.Date;
                              label5.Text = differentParameters.StartTime;
                             label6.Text = differentParameters.Interval;

                            break;
                        }
                    case "HRData":
                        {


                            //using the loop to till the end of array to get values 
                            for (int j = lineno + 1; j < LinesOfFile.Length; j++)
                            {
                                //Spliting chars with tabs
                                string[] newline = LinesOfFile[j].Split('\t');


                                //Switching different versions 
                                switch (differentParameters.Version)
                                {

                                    case "105":
                                        {
                                            hr.Add(new hrdata
                                            {
                                                HeartRate = int.Parse(newline[0]),
                                                Speed = int.Parse(newline[1]),
                                                Cadence = int.Parse(newline[2])
                                            });

                                            break;
                                        }
                                    case "106":
                                        {


                                            //adding the values 
                                            hr.Add(new hrdata
                                            {
                                                HeartRate = int.Parse(newline[0]),
                                                Speed = double.Parse(newline[1]) * 0.1,
                                                Cadence = int.Parse(newline[2]),
                                                Altitude = int.Parse(newline[3]),
                                                Power = int.Parse(newline[4]),
                                                PowerBalancePedalling = int.Parse(newline[5])
                                            });

                                            break;
                                        }
                                    case "107":
                                        {
                                            //adding the values to the parameters
                                            hr.Add(new hrdata
                                            {
                                                HeartRate = int.Parse(newline[0]),
                                                Speed = double.Parse(newline[1]) * 0.1,
                                                Cadence = int.Parse(newline[2]),
                                                Altitude = int.Parse(newline[3]),
                                                Power = int.Parse(newline[4]),
                                                PowerBalancePedalling = int.Parse(newline[5]),
                                                AirPressure = int.Parse(newline[6])
                                            });
                                            break;
                                        }
                                }
                            }
                            break;
                        }

                }
            }
            

            //putting column name is data grid view as per version of parmas
            string[] columnNames = { " HeartRate", " Speed", " Cadence", " Altitude", " Power", "PowerBalancePedalling" };


            foreach (string col in columnNames)
            {
                dt.Columns.Add(col);
            }


            foreach (hrdata hd in hr)
            {



                dt.Rows.Add(hd.HeartRate, hd.Speed, hd.Cadence, hd.Altitude, hd.Power, hd.PowerBalancePedalling);
            }

            //variables initiated for heart rate
            int minHeartRate = 1000;
            int maxHeartRate = 0, sum = 0;
            int count = 0;

            //variables for altitude ,power,speed

            int minAltitude = 1000;
            int maxAltitude = 0;
            int minPower = 1000;
            int maxPower = 0;
            double minSpeed = 1000;
            double maxSpeed = 0;
            //calculating the data to find maximum,minimum

            foreach (hrdata value in hr)
            {
                //for heart rate
                int hrValue = value.HeartRate;
                if (hrValue < minHeartRate)
                {
                    minHeartRate = hrValue;
                }
                else if (hrValue > maxHeartRate)
                {
                    maxHeartRate = hrValue;
                }
                sum += hrValue;
                count++;
                //for altitude 
                int altValue = value.Altitude;
                if (altValue < minAltitude)
                {
                    minAltitude = altValue;
                }
                else if (altValue > maxAltitude)
                {
                    maxAltitude = altValue;
                }

                //for power
                int PowerValue = value.Power;
                if (PowerValue < minPower)
                {
                    minPower = PowerValue;
                }
                else if (altValue > maxPower)
                {
                    maxPower = PowerValue;
                }
                //speed

                double SpeedValue = value.Speed;
                if (SpeedValue < minSpeed)
                {
                    minSpeed = SpeedValue;
                }
                else if (SpeedValue > maxSpeed)
                {
                    maxSpeed = SpeedValue;
                }


            }
            //calculating the stats of heart rate
            label7.Text ="Average Heart Rate::"+ (sum / count).ToString();
            label8.Text ="Maximum Heart Rate::"+maxHeartRate.ToString();
            label9.Text = "Minimum Heart Rate::" + minHeartRate.ToString();
            
            //calculating for altitude ,power ,speed
            label10.Text ="Maximun Power::" + maxPower.ToString()+"Watt";
            label11.Text ="Minimum Power::" +minPower.ToString()+"Watt";

            label12.Text = "Maximum Altitude::" + maxAltitude.ToString()+"Meter";
            label13.Text = "Minimum Altitude::" + minAltitude.ToString()+"Meter";

            label14.Text = "Maximum Speed::" + maxSpeed.ToString()+"Km/Hr";
            label15.Text = "Minimum Speed::" + minSpeed.ToString() + "Km/Hr";


            ParseData.DataSource = dt;
        }
        public void heartrate()
        {
            


        }

        private void ParseData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void ParseData_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void staticGraphToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            graph gr = new graph();
            gr.setHR(hr);
            gr.Show();
        }

        void FillData()
        {
            // 1
            // Open connection
            using (SqlCeConnection c = new SqlCeConnection(
                Properties.Settings.Default.DataConnectionString))
            {
                c.Open();
                // 2
                // Create new DataAdapter
                using (SqlCeDataAdapter a = new SqlCeDataAdapter(
                    "SELECT * FROM Animals", c))
                {
                    // 3
                    // Use DataAdapter to fill DataTable
                    DataTable t =using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolarHrmData_Assignment1
{
    public partial class interval : Form
    {
        public interval()
        {
            InitializeComponent();
        }

        string file_name;

        public void getfile_name(string file)
        {
            this.file_name = file;
        }

        private void interval_Load(object sender, EventArgs e)
        {
            int[] power,power2,power3,power4;
            string txtData = File.ReadAllText(file_name);
            string txtDataname = File.ReadAllText("ASDBExampleCycleComputerData.hrm");
            int count = 0, count1 = 0, count3 = 0, count4 = 0;
            double powertotal = 0, powertotal2 = 0, powertotal3 = 0, powertotal4 = 0;
                
            
            //For Session 1------------

                
                string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                int in_val = Array.IndexOf(arrData, "[HRData]");

                power = new int[arrData.Length - (in_val + 1)];

                for (int i = in_val+ 1; i < 147; i++)
                {
                    string HRData = arrData[i];
                    string[] arrHrdata = Regex.Split(HRData, @"\W+");

                    power[count] = Convert.ToInt32(arrHrdata[4]);

                    //Total power1
                    double pwr = power[count];
                    powertotal = powertotal + pwr;

                    count++;
                }

                //first session average power calculation
                double avgpwr1 = Math.Round(powertotal / count);
                this.label1.Text = "Average Power: " +avgpwr1.ToString();

                //first session high power calculation
                int maxpwr1 = power.Max();
                this.label2.Text = "Maximum Power: " +maxpwr1.ToString();

                //For Session 2----------------------------------------
               
                string[] arrData2 = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                int in_val2 = Array.IndexOf(arrData2, "[HRData]");

                power2 = new int[arrData2.Length - (in_val2 + 1)];


                for (int i = in_val2 + 35; i < 723; i++)
                {
                    string HRData = arrData2[i];
                    string[] arrHrdata = Regex.Split(HRData, @"\W+");

                    power2[count1] = Convert.ToInt32(arrHrdata[4]);

                    //Total power2
                    double pwr2 = power2[count1];
                    powertotal2 = powertotal2 + pwr2;
                    count1++;
                }

                //second session average power calculation
                double avgpwr2 = Math.Round(powertotal2 / count1);
                this.label3.Text = "Average Power: " +avgpwr2.ToString();

                //second session average power calculation
                int maxpwr2 = power2.Max();
                this.label4.Text = "MaximumPower: " +maxpwr2.ToString();



            //For Session 3-------------------------------------

                    
                    string[] arrData3 = txtDataname.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                    int in_val3 = Array.IndexOf(arrData3, "[HRData]");

                    power3 = new int[arrData3.Length - (in_val3 + 1)];


                    for (int i = in_val3 + 608; i < 1339; i++)
                    {
                        string HRData = arrData3[i];
                        string[] arrHrdata = Regex.Split(HRData, @"\W+");

                        power3[count3] = Convert.ToInt32(arrHrdata[4]);

                        //Total power3
                        double pwr3 = power3[count3];
                        powertotal3 = powertotal3 + pwr3;
                        count3++;
                    }

                    //third session average power calculation
                    double avgpwr3 = Math.Round(powertotal3 / count3);
                    this.label5.Text = "Average Power: " +avgpwr3.ToString();

                    //third session average power calculation
                    int maxpwr3 = power3.Max();
                    this.label6.Text = "Maximum Power: " +maxpwr3.ToString();


            //For Session 4----------------------------------------

                    
                    string[] arrData4 = txtDataname.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                    int in_val4 = Array.IndexOf(arrData4, "[HRData]");

                    power4 = new int[arrData4.Length - (in_val4 + 1)];

                    for (int i = in_val4 + 1510; i < 2245; i++)
                    {
                        string HRData = arrData4[i];
                        string[] arrHrdata = Regex.Split(HRData, @"\W+");

                        power4[count4] = Convert.ToInt32(arrHrdata[4]);

                        //Total power1
                        double pwr4 = power4[count4];
                        powertotal4 = powertotal4 + pwr4;
                        count4++;
                    }

                    //forth session average power calculation
                    double avgpwr4 = Math.Round(powertotal4 / count4);
                    this.label7.Text = "Average Power: " +avgpwr4.ToString();
            using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolarHrmData_Assignment1
{
    public partial class advance_metric : Form
    {
        public advance_metric()
        {
            InitializeComponent();
        }

        string file_name;

        public void getfile_name(string file)
        {
            this.file_name = file;
        }

        int[] LXPWR;
        int[] ALXPWR;
        int[] THAPAPWR;
        double avgpwr = 0;
        double avgthapapwr = 0;
        double avglxpwr = 0;

        private void advance_metric_Load(object sender, EventArgs e)
        {
            try
            {
                string txtData = File.ReadAllText(file_name);
                string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                int index2 = Array.IndexOf(arrData, "[HRData]");

                ALXPWR = new int[arrData.Length - (index2 + 1)];


                int j = 0;
                double powertotal = 0;

                for (int i = index2 + 1; i < arrData.Length; i++)
                {
                    string HRData = arrData[i];
                    string[] arrHrdata = Regex.Split(HRData, @"\W+");
                    ALXPWR[j] = Convert.ToInt32(arrHrdata[4]);

                    powertotal = powertotal + ALXPWR[j];
                    j++;
                }

                //for average power
                avgpwr = Math.Round(powertotal / (j));
                this.label12.Text = avgpwr.ToString("#.###") + " watts";
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}

                    //forth session average power calculation
                    int maxpwr4 = power4.Max();
                    this.label8.Text = "Maximum Power: " +maxpwr4.ToString();
        }
    }
}
 new DataTable();
                    a.Fill(t);
                    // 4
                    // Render data onto the screen
                    dataGridView1.DataSource = t;
                }
            }
        }
    }
}
    }
}

    }
}


        }
    }
}

}


        }
    }
}
