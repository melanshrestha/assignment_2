﻿namespace PolarHrmData_Assignment1
{
    partial class advance_metric
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFTP = new System.Windows.Forms.Label();
            this.lblIF = new System.Windows.Forms.Label();
            this.lblNP = new System.Windows.Forms.Label();
            this.lblTSS = new System.Windows.Forms.Label();
            this.lblAvgPower = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblFTP
            // 
            this.lblFTP.AutoSize = true;
            this.lblFTP.Location = new System.Drawing.Point(12, 41);
            this.lblFTP.Name = "lblFTP";
            this.lblFTP.Size = new System.Drawing.Size(139, 13);
            this.lblFTP.TabIndex = 7;
            this.lblFTP.Text = "Functional Threshold Power";
            // 
            // lblIF
            // 
            this.lblIF.AutoSize = true;
            this.lblIF.Location = new System.Drawing.Point(12, 72);
            this.lblIF.Name = "lblIF";
            this.lblIF.Size = new System.Drawing.Size(79, 13);
            this.lblIF.TabIndex = 8;
            this.lblIF.Text = "Intensity Factor";
            // 
            // lblNP
            // 
            this.lblNP.AutoSize = true;
            this.lblNP.Location = new System.Drawing.Point(12, 9);
            this.lblNP.Name = "lblNP";
            this.lblNP.Size = new System.Drawing.Size(92, 13);
            this.lblNP.TabIndex = 9;
            this.lblNP.Text = "Normalized Power";
            // 
            // lblTSS
            // 
            this.lblTSS.AutoSize = true;
            this.lblTSS.Location = new System.Drawing.Point(12, 105);
            this.lblTSS.Name = "lblTSS";
            this.lblTSS.Size = new System.Drawing.Size(28, 13);
            this.lblTSS.TabIndex = 10;
            this.lblTSS.Text = "TSS";
            // 
            // lblAvgPower
            // 
            this.lblAvgPower.AutoSize = true;
            this.lblAvgPower.Location = new System.Drawing.Point(11, 136);
            this.lblAvgPower.Name = "lblAvgPower";
            this.lblAvgPower.Size = new System.Drawing.Size(80, 13);
            this.lblAvgPower.TabIndex = 11;
            this.lblAvgPower.Text = "Average Power";
            // 
            // advance_metric
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(293, 170);
            this.Controls.Add(this.lblAvgPower);
            this.Controls.Add(this.lblTSS);
            this.Controls.Add(this.lblNP);
            this.Controls.Add(this.lblIF);
            this.Controls.Add(this.lblFTP);
            this.Name = "advance_metric";
            this.Text = "advance_metric";
            this.Load += new System.EventHandler(this.advance_metric_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFTP;
        private System.Windows.Forms.Label lblIF;
        private System.Windows.Forms.Label lblNP;
        private System.Windows.Forms.Label lblTSS;
        private System.Windows.Forms.Label lblAvgPower;
    }
}