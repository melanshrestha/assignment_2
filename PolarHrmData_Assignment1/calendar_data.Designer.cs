﻿namespace PolarHrmData_Assignment1
{
    partial class date_selected
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbldate = new System.Windows.Forms.Label();
            this.lbllength = new System.Windows.Forms.Label();
            this.startdate = new System.Windows.Forms.Label();
            this.lblmonitor = new System.Windows.Forms.Label();
            this.lblversion = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblsmode = new System.Windows.Forms.Label();
            this.file1 = new System.Windows.Forms.Label();
            this.lblinterval = new System.Windows.Forms.Label();
            this.advancemetrics = new Bunifu.Framework.UI.BunifuFlatButton();
            this.intervaldetection = new Bunifu.Framework.UI.BunifuFlatButton();
            this.dataselect = new Bunifu.Framework.UI.BunifuFlatButton();
            this.open_folder = new Bunifu.Framework.UI.BunifuFlatButton();
            this.calendar = new System.Windows.Forms.MonthCalendar();
            this.data_load = new System.Windows.Forms.ListBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.time_interval = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.heart_rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.speeds = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cadences = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.altitudes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Power_watt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.power_balance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pending_index = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.left_right_balance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.maxheartrate = new System.Windows.Forms.Label();
            this.minheartrate = new System.Windows.Forms.Label();
            this.avgheartrate = new System.Windows.Forms.Label();
            this.maxspeed = new System.Windows.Forms.Label();
            this.minspeed = new System.Windows.Forms.Label();
            this.avgspeed = new System.Windows.Forms.Label();
            this.maxpower = new System.Windows.Forms.Label();
            this.minpower = new System.Windows.Forms.Label();
            this.avgpower = new System.Windows.Forms.Label();
            this.maxaltitude = new System.Windows.Forms.Label();
            this.minaltitude = new System.Windows.Forms.Label();
            this.avgaltitude = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbldate
            // 
            this.lbldate.AutoSize = true;
            this.lbldate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldate.Location = new System.Drawing.Point(5, 120);
            this.lbldate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbldate.Name = "lbldate";
            this.lbldate.Size = new System.Drawing.Size(35, 16);
            this.lbldate.TabIndex = 67;
            this.lbldate.Text = "date";
            // 
            // lbllength
            // 
            this.lbllength.AutoSize = true;
            this.lbllength.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllength.Location = new System.Drawing.Point(5, 95);
            this.lbllength.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbllength.Name = "lbllength";
            this.lbllength.Size = new System.Drawing.Size(44, 16);
            this.lbllength.TabIndex = 65;
            this.lbllength.Text = "length";
            // 
            // startdate
            // 
            this.startdate.AutoSize = true;
            this.startdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startdate.Location = new System.Drawing.Point(5, 70);
            this.startdate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.startdate.Name = "startdate";
            this.startdate.Size = new System.Drawing.Size(63, 16);
            this.startdate.TabIndex = 64;
            this.startdate.Text = "start date";
            // 
            // lblmonitor
            // 
            this.lblmonitor.AutoSize = true;
            this.lblmonitor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmonitor.Location = new System.Drawing.Point(5, 44);
            this.lblmonitor.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblmonitor.Name = "lblmonitor";
            this.lblmonitor.Size = new System.Drawing.Size(52, 16);
            this.lblmonitor.TabIndex = 63;
            this.lblmonitor.Text = "monitor";
            // 
            // lblversion
            // 
            this.lblversion.AutoSize = true;
            this.lblversion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblversion.Location = new System.Drawing.Point(5, 20);
            this.lblversion.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblversion.Name = "lblversion";
            this.lblversion.Size = new System.Drawing.Size(52, 16);
            this.lblversion.TabIndex = 62;
            this.lblversion.Text = "version";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblsmode);
            this.groupBox1.Controls.Add(this.file1);
            this.groupBox1.Controls.Add(this.lblinterval);
            this.groupBox1.Controls.Add(this.lblversion);
            this.groupBox1.Controls.Add(this.lbldate);
            this.groupBox1.Controls.Add(this.lblmonitor);
            this.groupBox1.Controls.Add(this.startdate);
            this.groupBox1.Controls.Add(this.lbllength);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(183, 222);
            this.groupBox1.TabIndex = 68;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Header";
            // 
            // lblsmode
            // 
            this.lblsmode.AutoSize = true;
            this.lblsmode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsmode.Location = new System.Drawing.Point(6, 144);
            this.lblsmode.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblsmode.Name = "lblsmode";
            this.lblsmode.Size = new System.Drawing.Size(50, 16);
            this.lblsmode.TabIndex = 70;
            this.lblsmode.Text = "smode";
            // 
            // file1
            // 
            this.file1.AutoSize = true;
            this.file1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.file1.Location = new System.Drawing.Point(7, 191);
            this.file1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.file1.Name = "file1";
            this.file1.Size = new System.Drawing.Size(59, 16);
            this.file1.TabIndex = 69;
            this.file1.Text = "filename";
            // 
            // lblinterval
            // 
            this.lblinterval.AutoSize = true;
            this.lblinterval.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblinterval.Location = new System.Drawing.Point(7, 167);
            this.lblinterval.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblinterval.Name = "lblinterval";
            this.lblinterval.Size = new System.Drawing.Size(51, 16);
            this.lblinterval.TabIndex = 68;
            this.lblinterval.Text = "interval";
            // 
            // advancemetrics
            // 
            this.advancemetrics.Activecolor = System.Drawing.Color.CornflowerBlue;
            this.advancemetrics.BackColor = System.Drawing.Color.RoyalBlue;
            this.advancemetrics.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.advancemetrics.BorderRadius = 0;
            this.advancemetrics.ButtonText = "Advance Metrics";
            this.advancemetrics.Cursor = System.Windows.Forms.Cursors.Hand;
            this.advancemetrics.DisabledColor = System.Drawing.Color.Gray;
            this.advancemetrics.Iconcolor = System.Drawing.Color.Transparent;
            this.advancemetrics.Iconimage = null;
            this.advancemetrics.Iconimage_right = null;
            this.advancemetrics.Iconimage_right_Selected = null;
            this.advancemetrics.Iconimage_Selected = null;
            this.advancemetrics.IconMarginLeft = 0;
            this.advancemetrics.IconMarginRight = 0;
            this.advancemetrics.IconRightVisible = true;
            this.advancemetrics.IconRightZoom = 0D;
            this.advancemetrics.IconVisible = true;
            this.advancemetrics.IconZoom = 90D;
            this.advancemetrics.IsTab = false;
            this.advancemetrics.Location = new System.Drawing.Point(706, 12);
            this.advancemetrics.Name = "advancemetrics";
            this.advancemetrics.Normalcolor = System.Drawing.Color.RoyalBlue;
            this.advancemetrics.OnHovercolor = System.Drawing.Color.CornflowerBlue;
            this.advancemetrics.OnHoverTextColor = System.Drawing.Color.White;
            this.advancemetrics.selected = false;
            this.advancemetrics.Size = new System.Drawing.Size(168, 48);
            this.advancemetrics.TabIndex = 71;
            this.advancemetrics.Text = "Advance Metrics";
            this.advancemetrics.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.advancemetrics.Textcolor = System.Drawing.Color.White;
            this.advancemetrics.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.advancemetrics.Click += new System.EventHandler(this.advancemetrics_Click);
            // 
            // intervaldetection
            // 
            this.intervaldetection.Activecolor = System.Drawing.Color.CornflowerBlue;
            this.intervaldetection.BackColor = System.Drawing.Color.RoyalBlue;
            this.intervaldetection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.intervaldetection.BorderRadius = 0;
            this.intervaldetection.ButtonText = "Interval";
            this.intervaldetection.Cursor = System.Windows.Forms.Cursors.Hand;
            this.intervaldetection.DisabledColor = System.Drawing.Color.Gray;
            this.intervaldetection.Iconcolor = System.Drawing.Color.Transparent;
            this.intervaldetection.Iconimage = null;
            this.intervaldetection.Iconimage_right = null;
            this.intervaldetection.Iconimage_right_Selected = null;
            this.intervaldetection.Iconimage_Selected = null;
            this.intervaldetection.IconMarginLeft = 0;
            this.intervaldetection.IconMarginRight = 0;
            this.intervaldetection.IconRightVisible = true;
            this.intervaldetection.IconRightZoom = 0D;
            this.intervaldetection.IconVisible = true;
            this.intervaldetection.IconZoom = 90D;
            this.intervaldetection.IsTab = false;
            this.intervaldetection.Location = new System.Drawing.Point(958, 12);
            this.intervaldetection.Name = "intervaldetection";
            this.intervaldetection.Normalcolor = System.Drawing.Color.RoyalBlue;
            this.intervaldetection.OnHovercolor = System.Drawing.Color.CornflowerBlue;
            this.intervaldetection.OnHoverTextColor = System.Drawing.Color.White;
            this.intervaldetection.selected = false;
            this.intervaldetection.Size = new System.Drawing.Size(168, 48);
            this.intervaldetection.TabIndex = 72;
            this.intervaldetection.Text = "Interval";
            this.intervaldetection.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.intervaldetection.Textcolor = System.Drawing.Color.White;
            this.intervaldetection.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.intervaldetection.Click += new System.EventHandler(this.intervaldetection_Click);
            // 
            // dataselect
            // 
            this.dataselect.Activecolor = System.Drawing.Color.CornflowerBlue;
            this.dataselect.BackColor = System.Drawing.Color.RoyalBlue;
            this.dataselect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.dataselect.BorderRadius = 0;
            this.dataselect.ButtonText = "Data Select";
            this.dataselect.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dataselect.DisabledColor = System.Drawing.Color.Gray;
            this.dataselect.Iconcolor = System.Drawing.Color.Transparent;
            this.dataselect.Iconimage = null;
            this.dataselect.Iconimage_right = null;
            this.dataselect.Iconimage_right_Selected = null;
            this.dataselect.Iconimage_Selected = null;
            this.dataselect.IconMarginLeft = 0;
            this.dataselect.IconMarginRight = 0;
            this.dataselect.IconRightVisible = true;
            this.dataselect.IconRightZoom = 0D;
            this.dataselect.IconVisible = true;
            this.dataselect.IconZoom = 90D;
            this.dataselect.IsTab = false;
            this.dataselect.Location = new System.Drawing.Point(457, 12);
            this.dataselect.Name = "dataselect";
            this.dataselect.Normalcolor = System.Drawing.Color.RoyalBlue;
            this.dataselect.OnHovercolor = System.Drawing.Color.CornflowerBlue;
            this.dataselect.OnHoverTextColor = System.Drawing.Color.White;
            this.dataselect.selected = false;
            this.dataselect.Size = new System.Drawing.Size(168, 48);
            this.dataselect.TabIndex = 73;
            this.dataselect.Text = "Data Select";
            this.dataselect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.dataselect.Textcolor = System.Drawing.Color.White;
            this.dataselect.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataselect.Click += new System.EventHandler(this.dataselect_Click);
            // 
            // open_folder
            // 
            this.open_folder.Activecolor = System.Drawing.Color.CornflowerBlue;
            this.open_folder.BackColor = System.Drawing.Color.RoyalBlue;
            this.open_folder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.open_folder.BorderRadius = 0;
            this.open_folder.ButtonText = "Open Folder";
            this.open_folder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.open_folder.DisabledColor = System.Drawing.Color.Gray;
            this.open_folder.Iconcolor = System.Drawing.Color.Transparent;
            this.open_folder.Iconimage = null;
            this.open_folder.Iconimage_right = null;
            this.open_folder.Iconimage_right_Selected = null;
            this.open_folder.Iconimage_Selected = null;
            this.open_folder.IconMarginLeft = 0;
            this.open_folder.IconMarginRight = 0;
            this.open_folder.IconRightVisible = true;
            this.open_folder.IconRightZoom = 0D;
            this.open_folder.IconVisible = true;
            this.open_folder.IconZoom = 90D;
            this.open_folder.IsTab = false;
            this.open_folder.Location = new System.Drawing.Point(207, 12);
            this.open_folder.Name = "open_folder";
            this.open_folder.Normalcolor = System.Drawing.Color.RoyalBlue;
            this.open_folder.OnHovercolor = System.Drawing.Color.CornflowerBlue;
            this.open_folder.OnHoverTextColor = System.Drawing.Color.White;
            this.open_folder.selected = false;
            this.open_folder.Size = new System.Drawing.Size(168, 48);
            this.open_folder.TabIndex = 75;
            this.open_folder.Text = "Open Folder";
            this.open_folder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.open_folder.Textcolor = System.Drawing.Color.White;
            this.open_folder.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.open_folder.Click += new System.EventHandler(this.open_folder_Click);
            // 
            // calendar
            // 
            this.calendar.Location = new System.Drawing.Point(207, 72);
            this.calendar.Name = "calendar";
            this.calendar.TabIndex = 76;
            this.calendar.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.calendar_DateChanged);
            // 
            // data_load
            // 
            this.data_load.FormattingEnabled = true;
            this.data_load.Location = new System.Drawing.Point(12, 246);
            this.data_load.Name = "data_load";
            this.data_load.Size = new System.Drawing.Size(419, 186);
            this.data_load.TabIndex = 77;
            this.data_load.MouseClick += new System.Windows.Forms.MouseEventHandler(this.data_load_MouseClick);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.time_interval,
            this.heart_rate,
            this.speeds,
            this.cadences,
            this.altitudes,
            this.Power_watt,
            this.power_balance,
            this.pending_index,
            this.left_right_balance});
            this.dataGridView1.Location = new System.Drawing.Point(437, 72);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(689, 360);
            this.dataGridView1.TabIndex = 78;
            this.dataGridView1.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseUp);
            // 
            // time_interval
            // 
            this.time_interval.HeaderText = "TIme Interval";
            this.time_interval.Name = "time_interval";
            this.time_interval.ReadOnly = true;
            // 
            // heart_rate
            // 
            this.heart_rate.HeaderText = "Heart Rate";
            this.heart_rate.Name = "heart_rate";
            this.heart_rate.ReadOnly = true;
            // 
            // speeds
            // 
            this.speeds.HeaderText = "Speed";
            this.speeds.Name = "speeds";
            this.speeds.ReadOnly = true;
            // 
            // cadences
            // 
            this.cadences.HeaderText = "Cadence";
            this.cadences.Name = "cadences";
            this.cadences.ReadOnly = true;
            // 
            // altitudes
            // 
            this.altitudes.HeaderText = "Altitude";
            this.altitudes.Name = "altitudes";
            this.altitudes.ReadOnly = true;
            // 
            // Power_watt
            // 
            this.Power_watt.HeaderText = "Power(Watt)";
            this.Power_watt.Name = "Power_watt";
            this.Power_watt.ReadOnly = true;
            // 
            // power_balance
            // 
            this.power_balance.HeaderText = "Power Balance";
            this.power_balance.Name = "power_balance";
            this.power_balance.ReadOnly = true;
            // 
            // pending_index
            // 
            this.pending_index.HeaderText = "Pending Index";
            this.pending_index.Name = "pending_index";
            this.pending_index.ReadOnly = true;
            // 
            // left_right_balance
            // 
            this.left_right_balance.HeaderText = "Left Right Balance";
            this.left_right_balance.Name = "left_right_balance";
            this.left_right_balance.ReadOnly = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.avgaltitude);
            this.groupBox2.Controls.Add(this.minaltitude);
            this.groupBox2.Controls.Add(this.maxaltitude);
            this.groupBox2.Controls.Add(this.avgpower);
            this.groupBox2.Controls.Add(this.minpower);
            this.groupBox2.Controls.Add(this.maxpower);
            this.groupBox2.Controls.Add(this.avgspeed);
            this.groupBox2.Controls.Add(this.minspeed);
            this.groupBox2.Controls.Add(this.maxspeed);
            this.groupBox2.Controls.Add(this.avgheartrate);
            this.groupBox2.Controls.Add(this.minheartrate);
            this.groupBox2.Controls.Add(this.maxheartrate);
            this.groupBox2.Location = new System.Drawing.Point(1132, 72);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(218, 360);
            this.groupBox2.TabIndex = 79;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Data Summary";
            // 
            // maxheartrate
            // 
            this.maxheartrate.AutoSize = true;
            this.maxheartrate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxheartrate.Location = new System.Drawing.Point(5, 20);
            this.maxheartrate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.maxheartrate.Name = "maxheartrate";
            this.maxheartrate.Size = new System.Drawing.Size(101, 16);
            this.maxheartrate.TabIndex = 62;
            this.maxheartrate.Text = "Max Heart Rate";
            // 
            // minheartrate
            // 
            this.minheartrate.AutoSize = true;
            this.minheartrate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minheartrate.Location = new System.Drawing.Point(5, 47);
            this.minheartrate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.minheartrate.Name = "minheartrate";
            this.minheartrate.Size = new System.Drawing.Size(97, 16);
            this.minheartrate.TabIndex = 63;
            this.minheartrate.Text = "Min Heart Rate";
            // 
            // avgheartrate
            // 
            this.avgheartrate.AutoSize = true;
            this.avgheartrate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgheartrate.Location = new System.Drawing.Point(5, 75);
            this.avgheartrate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.avgheartrate.Name = "avgheartrate";
            this.avgheartrate.Size = new System.Drawing.Size(128, 16);
            this.avgheartrate.TabIndex = 64;
            this.avgheartrate.Text = "Average Heart Rate";
            // 
            // maxspeed
            // 
            this.maxspeed.AutoSize = true;
            this.maxspeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxspeed.Location = new System.Drawing.Point(5, 103);
            this.maxspeed.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.maxspeed.Name = "maxspeed";
            this.maxspeed.Size = new System.Drawing.Size(77, 16);
            this.maxspeed.TabIndex = 65;
            this.maxspeed.Text = "Max Speed";
            // 
            // minspeed
            // 
            this.minspeed.AutoSize = true;
            this.minspeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minspeed.Location = new System.Drawing.Point(5, 130);
            this.minspeed.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.minspeed.Name = "minspeed";
            this.minspeed.Size = new System.Drawing.Size(73, 16);
            this.minspeed.TabIndex = 66;
            this.minspeed.Text = "Min Speed";
            // 
            // avgspeed
            // 
            this.avgspeed.AutoSize = true;
            this.avgspeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgspeed.Location = new System.Drawing.Point(5, 157);
            this.avgspeed.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.avgspeed.Name = "avgspeed";
            this.avgspeed.Size = new System.Drawing.Size(104, 16);
            this.avgspeed.TabIndex = 67;
            this.avgspeed.Text = "Average Speed";
            // 
            // maxpower
            // 
            this.maxpower.AutoSize = true;
            this.maxpower.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxpower.Location = new System.Drawing.Point(5, 187);
            this.maxpower.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.maxpower.Name = "maxpower";
            this.maxpower.Size = new System.Drawing.Size(74, 16);
            this.maxpower.TabIndex = 68;
            this.maxpower.Text = "Max Power";
            // 
            // minpower
            // 
            this.minpower.AutoSize = true;
            this.minpower.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minpower.Location = new System.Drawing.Point(5, 216);
            this.minpower.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.minpower.Name = "minpower";
            this.minpower.Size = new System.Drawing.Size(70, 16);
            this.minpower.TabIndex = 69;
            this.minpower.Text = "Min Power";
            // 
            // avgpower
            // 
            this.avgpower.AutoSize = true;
            this.avgpower.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgpower.Location = new System.Drawing.Point(5, 245);
            this.avgpower.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.avgpower.Name = "avgpower";
            this.avgpower.Size = new System.Drawing.Size(101, 16);
            this.avgpower.TabIndex = 70;
            this.avgpower.Text = "Average Power";
            // 
            // maxaltitude
            // 
            this.maxaltitude.AutoSize = true;
            this.maxaltitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxaltitude.Location = new System.Drawing.Point(5, 275);
            this.maxaltitude.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.maxaltitude.Name = "maxaltitude";
            this.maxaltitude.Size = new System.Drawing.Size(80, 16);
            this.maxaltitude.TabIndex = 71;
            this.maxaltitude.Text = "Max Altitude";
            // 
            // minaltitude
            // 
            this.minaltitude.AutoSize = true;
            this.minaltitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minaltitude.Location = new System.Drawing.Point(5, 304);
            this.minaltitude.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.minaltitude.Name = "minaltitude";
            this.minaltitude.Size = new System.Drawing.Size(76, 16);
            this.minaltitude.TabIndex = 72;
            this.minaltitude.Text = "Min Altitude";
            // 
            // avgaltitude
            // 
            this.avgaltitude.AutoSize = true;
            this.avgaltitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgaltitude.Location = new System.Drawing.Point(5, 334);
            this.avgaltitude.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.avgaltitude.Name = "avgaltitude";
            this.avgaltitude.Size = new System.Drawing.Size(107, 16);
            this.avgaltitude.TabIndex = 73;
            this.avgaltitude.Text = "Average Altitude";
            // 
            // date_selected
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1362, 454);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.data_load);
            this.Controls.Add(this.calendar);
            this.Controls.Add(this.open_folder);
            this.Controls.Add(this.dataselect);
            this.Controls.Add(this.intervaldetection);
            this.Controls.Add(this.advancemetrics);
            this.Controls.Add(this.groupBox1);
            this.Name = "date_selected";
            this.Text = "Date_selected";
            this.Load += new System.EventHandler(this.Date_selected_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbldate;
        //private System.Windows.Forms.Label smode;
        private System.Windows.Forms.Label lbllength;
        private System.Windows.Forms.Label startdate;
        private System.Windows.Forms.Label lblmonitor;
        private System.Windows.Forms.Label lblversion;
        private System.Windows.Forms.GroupBox groupBox1;
        private Bunifu.Framework.UI.BunifuFlatButton advancemetrics;
        private Bunifu.Framework.UI.BunifuFlatButton intervaldetection;
        private Bunifu.Framework.UI.BunifuFlatButton dataselect;
        private System.Windows.Forms.Label lblinterval;
       //public System.Windows.Forms.Label filename;
        private System.Windows.Forms.Label file1;
        private Bunifu.Framework.UI.BunifuFlatButton open_folder;
        private System.Windows.Forms.MonthCalendar calendar;
        private System.Windows.Forms.ListBox data_load;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn time_interval;
        private System.Windows.Forms.DataGridViewTextBoxColumn heart_rate;
        private System.Windows.Forms.DataGridViewTextBoxColumn speeds;
        private System.Windows.Forms.DataGridViewTextBoxColumn cadences;
        private System.Windows.Forms.DataGridViewTextBoxColumn altitudes;
        private System.Windows.Forms.DataGridViewTextBoxColumn Power_watt;
        private System.Windows.Forms.DataGridViewTextBoxColumn power_balance;
        private System.Windows.Forms.DataGridViewTextBoxColumn pending_index;
        private System.Windows.Forms.DataGridViewTextBoxColumn left_right_balance;
        private System.Windows.Forms.Label lblsmode;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label maxheartrate;
        private System.Windows.Forms.Label avgaltitude;
        private System.Windows.Forms.Label minaltitude;
        private System.Windows.Forms.Label maxaltitude;
        private System.Windows.Forms.Label avgpower;
        private System.Windows.Forms.Label minpower;
        private System.Windows.Forms.Label maxpower;
        private System.Windows.Forms.Label avgspeed;
        private System.Windows.Forms.Label minspeed;
        private System.Windows.Forms.Label maxspeed;
        private System.Windows.Forms.Label avgheartrate;
        private System.Windows.Forms.Label minheartrate;
    }
}