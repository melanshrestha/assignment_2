﻿namespace PolarHrmData_Assignment1
{
    partial class data_selection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.time_interval = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.heart_rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.speeds = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cadences = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.altitudes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Power_watt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.power_balance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pending_index = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.left_right_balance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // zedGraphControl1
            // 
            this.zedGraphControl1.Location = new System.Drawing.Point(499, 3);
            this.zedGraphControl1.Name = "zedGraphControl1";
            this.zedGraphControl1.ScrollGrace = 0D;
            this.zedGraphControl1.ScrollMaxX = 0D;
            this.zedGraphControl1.ScrollMaxY = 0D;
            this.zedGraphControl1.ScrollMaxY2 = 0D;
            this.zedGraphControl1.ScrollMinX = 0D;
            this.zedGraphControl1.ScrollMinY = 0D;
            this.zedGraphControl1.ScrollMinY2 = 0D;
            this.zedGraphControl1.Size = new System.Drawing.Size(500, 366);
            this.zedGraphControl1.TabIndex = 2;
            this.zedGraphControl1.UseExtendedPrintDialog = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.time_interval,
            this.heart_rate,
            this.speeds,
            this.cadences,
            this.altitudes,
            this.Power_watt,
            this.power_balance,
            this.pending_index,
            this.left_right_balance});
            this.dataGridView1.Location = new System.Drawing.Point(12, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(490, 318);
            this.dataGridView1.TabIndex = 79;
            this.dataGridView1.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseUp);
            // 
            // time_interval
            // 
            this.time_interval.HeaderText = "TIme Interval";
            this.time_interval.Name = "time_interval";
            this.time_interval.ReadOnly = true;
            this.time_interval.Visible = false;
            // 
            // heart_rate
            // 
            this.heart_rate.HeaderText = "Heart Rate";
            this.heart_rate.Name = "heart_rate";
            this.heart_rate.ReadOnly = true;
            // 
            // speeds
            // 
            this.speeds.HeaderText = "Speed";
            this.speeds.Name = "speeds";
            this.speeds.ReadOnly = true;
            // 
            // cadences
            // 
            this.cadences.HeaderText = "Cadence";
            this.cadences.Name = "cadences";
            this.cadences.ReadOnly = true;
            // 
            // altitudes
            // 
            this.altitudes.HeaderText = "Altitude";
            this.altitudes.Name = "altitudes";
            this.altitudes.ReadOnly = true;
            // 
            // Power_watt
            // 
            this.Power_watt.HeaderText = "Power(Watt)";
            this.Power_watt.Name = "Power_watt";
            this.Power_watt.ReadOnly = true;
            // 
            // power_balance
            // 
            this.power_balance.HeaderText = "Power Balance";
            this.power_balance.Name = "power_balance";
            this.power_balance.ReadOnly = true;
            this.power_balance.Visible = false;
            // 
            // pending_index
            // 
            this.pending_index.HeaderText = "Pending Index";
            this.pending_index.Name = "pending_index";
            this.pending_index.ReadOnly = true;
            this.pending_index.Visible = false;
            // 
            // left_right_balance
            // 
            this.left_right_balance.HeaderText = "Left Right Balance";
            this.left_right_balance.Name = "left_right_balance";
            this.left_right_balance.ReadOnly = true;
            this.left_right_balance.Visible = false;
            // 
            // data_selection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(988, 375);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.zedGraphControl1);
            this.Name = "data_selection";
            this.Text = "data_selection";
            this.Load += new System.EventHandler(this.data_selection_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ZedGraph.ZedGraphControl zedGraphControl1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn time_interval;
        private System.Windows.Forms.DataGridViewTextBoxColumn heart_rate;
        private System.Windows.Forms.DataGridViewTextBoxColumn speeds;
        private System.Windows.Forms.DataGridViewTextBoxColumn cadences;
        private System.Windows.Forms.DataGridViewTextBoxColumn altitudes;
        private System.Windows.Forms.DataGridViewTextBoxColumn Power_watt;
        private System.Windows.Forms.DataGridViewTextBoxColumn power_balance;
        private System.Windows.Forms.DataGridViewTextBoxColumn pending_index;
        private System.Windows.Forms.DataGridViewTextBoxColumn left_right_balance;
    }
}