﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolarHrmData_Assignment1
{
    public partial class Form1 : Form
    {
        private int seconds; private int counter; private int NumberOfLines; private string heart;private string speed; private string cadence;// = string.Empty;private string altitude;
        private string power; private string powerbal; private string line; private string HData; private double[] hgraph; private string altitude; DateTime myDateTime;
        int lineNo; 

        string lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8, lb9, lb10, lb11, lb12;

        public Form1()
        {
            InitializeComponent();
        }
        
        
        List<header> headVal = new List<header>();
        public List<hrdata> hr = new List<hrdata>();
        
        parameters allParameters = new parameters();

        DataTable dt = new DataTable();
        private string[] Filelines;
        
        private void Form1_Load(object sender, EventArgs e)
        {
            bunifuFlatButton1.Visible = false;
            dataGridView1.Visible = false;
            cycleInfo.Visible = false;
            label7.Visible = false;
            label8.Visible = false;
            label9.Visible = false;
            label10.Visible = false;
            label11.Visible = false;
            label12.Visible = false;
            label14.Visible = false;
            label15.Visible = false;
            label22.Visible = false;
            label23.Visible = false;
            label24.Visible = false;
            label25.Visible = false;
            label26.Visible = false;
            label27.Visible = false;
            label28.Visible = false;
            label29.Visible = false;
            label31.Visible = false;
            label32.Visible = false;
            label33.Visible = false;
            label34.Visible = false;
            label35.Visible = false;
            label36.Visible = false;
            label37.Visible = false;
            label38.Visible = false;
            label36.Visible = false;
            label16.Visible = false;
            label17.Visible = false;
            label18.Visible = false;
            label19.Visible = false;
            label20.Visible = false;
            label21.Visible = false;
            label1.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
        }

        private void browseFile_Click_1(object sender, EventArgs e)
        {
            using (OpenFileDialog browseFile = new OpenFileDialog())
            {
                if (browseFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string filename = browseFile.FileName;
                    //Reading the File

                    Filelines = File.ReadAllLines(filename);
                    
                    
                    for (int linecount = 0; linecount < Filelines.Length; linecount++)
                    {
                        //checking file if is null
                        if (Filelines[linecount].Length > 0)
                        {
                           
                            if (Filelines[linecount][0] == '[' && Filelines[linecount][Filelines[linecount].Length - 1] == ']')
                            {
                                
                                headVal.Add(new header { headername = Filelines[linecount].Substring(1, Filelines[linecount].Length - 2), headerline = linecount });
                                //cycleInfo.Visible = true;
                                label7.Visible = true;
                                label8.Visible = true;
                                label9.Visible = true;
                                label10.Visible = true;
                                label11.Visible = true;
                                label12.Visible = true;
                                label14.Visible = true;
                                label15.Visible = true;
                                label22.Visible = true;
                                label23.Visible = true;
                                label24.Visible = true;
                                label25.Visible = true;
                                label26.Visible = true;
                                label27.Visible = true;
                                label28.Visible = true;
                                label29.Visible = true;
                                label31.Visible = true;
                                label32.Visible = true;
                                label33.Visible = true;
                                label34.Visible = true;
                                label35.Visible = true;
                                label36.Visible = true;
                                label37.Visible = true;
                                label38.Visible = true;
                                label36.Visible = true;
                                label16.Visible = true;
                                label17.Visible = true;
                                label18.Visible = true;
                                label19.Visible = true;
                                label20.Visible = true;
                                label21.Visible = true;
                                label1.Visible = true;
                                label2.Visible = true;
                                label3.Visible = true;
                                bunifuFlatButton1.Visible = true;
                            }
                        }
                    }

                    StreamReader reader = new StreamReader(browseFile.FileName, System.Text.Encoding.Default);
                    string readFileText = reader.ReadToEnd();

                    //read from the textfile
                    int lineIndex = readFileText.IndexOf("Date=");
                    string dateLines = readFileText.Substring(lineIndex + 5, 8);

                    //for reading the date from the text file
                    dateLines = dateLines.Insert(4, "-"); 
                    dateLines = dateLines.Insert(7, "-"); 

                    //read text with certain symbols
                    lineIndex = readFileText.IndexOf("StartTime=");
                    string lineTime = readFileText.Substring(lineIndex + 10, 8); // for the time with hour minute and seconds
                    lineIndex = readFileText.IndexOf("Interval=");
                    string lineinter1 = readFileText.Substring(lineIndex + 9, 1); // for the time with hour minute and seconds 
                    lineIndex = readFileText.IndexOf("Length=");
                    string lineLength = readFileText.Substring(lineIndex + 7, 8);  // for the time with hour minute and seconds


                    string textTimeStamp = dateLines + "   " + lineTime;
                    StreamReader sr = new StreamReader(browseFile.FileName, System.Text.Encoding.Default);
                    HData = null;
                    NumberOfLines = File.ReadAllLines(browseFile.FileName).Length;
                    while ((HData = sr.ReadLine()) != null)
                    {
                        if (HData.IndexOf("[HRData]") != -1)
                        {
                            //found it
                            break;
                        }
                    }
                    line = sr.ReadLine();
                    // Setup an accumulator
                    int heartTotal = 0, speedTotal = 0, powerTotal = 0, altTotal = 0;
                    double mph = 0;

                    DataGridViewColumn timestamp = new DataGridViewTextBoxColumn();
                    timestamp.HeaderText = "Time Stamp";
                    int dataCol1 = dataGridView1.Columns.Add(timestamp);

                    DataGridViewColumn heartrate = new DataGridViewTextBoxColumn();
                    heartrate.HeaderText = "Heart Rate";
                    int dataCol2 = dataGridView1.Columns.Add(heartrate);

                    DataGridViewColumn speedval = new DataGridViewTextBoxColumn();
                    speedval.HeaderText = "Speed";
                    int dataCol3 = dataGridView1.Columns.Add(speedval);

                    DataGridViewColumn cadencer = new DataGridViewTextBoxColumn();
                    cadencer.HeaderText = "Cadence";
                    int dataCol4 = dataGridView1.Columns.Add(cadencer);

                    DataGridViewColumn altit = new DataGridViewTextBoxColumn();
                    altit.HeaderText = "Altitude";
                    int dataCol5 = dataGridView1.Columns.Add(altit);

                    DataGridViewColumn pwr = new DataGridViewTextBoxColumn();
                    pwr.HeaderText = "Power (watts)";
                    int dataCol6 = dataGridView1.Columns.Add(pwr);

                    DataGridViewColumn pwrbal = new DataGridViewTextBoxColumn();
                    pwrbal.HeaderText = "Power Balance";
                    int colIndex7 = dataGridView1.Columns.Add(pwrbal);

                    DataGridViewColumn moveavg = new DataGridViewTextBoxColumn();
                    moveavg.HeaderText = "Moving Avg";
                    int colIndex8 = dataGridView1.Columns.Add(moveavg);

                    

                    seconds = Convert.ToInt32(lineinter1);
                    counter = 0;
                    hgraph = new double[0];
                    while (line != null)
                    {
                        List<string> heartarray = new List<string>();

                        
                        counter++;
                        if (seconds <= 1)
                        {
                            myDateTime = myDateTime.AddSeconds(seconds);
                        }
                        else
                        {
                            if (seconds >= 1)
                                myDateTime = myDateTime.AddSeconds(seconds);
                                }
                        heart = line.Split('\t')[0];
                        int heartint = Convert.ToInt32(heart);
                        
                        heartarray.Add(heart);
                        heartTotal += heartint;
                        speed = line.Split('\t')[1];
                        int speedint = Convert.ToInt32(speed);
                        speedTotal += speedint;
                        mph = ((double)speedint / (double)1.6);

                        if (speedint <= 1)
                        {
                            speed = speed.Insert(1, "."); 
                        }
                        else
                        {
                            if (speedint >= 1)
                                speed = speed.Insert(2, "."); 
                        }
                        cadence = line.Split('\t')[2];
                        int cadenceint = Convert.ToInt32(cadence);
                        altitude = line.Split('\t')[3];
                        int altitudeint = Convert.ToInt32(altitude);
                        altTotal += altitudeint;
                        power = line.Split('\t')[4];

                        powerbal = line.Split('\t')[5];
                        int Powerbalint = Convert.ToInt32(powerbal);
                        int powerint = Convert.ToInt32(power);
                        powerTotal += powerint;
                        line = sr.ReadLine();
                        dataGridView1.Rows.Add(myDateTime, heart, speed, cadence, altitude, power, powerbal);

                           
                    }
                    int[] powerlist = new int[] { Convert.ToInt32(powerbal) };

                    for (int i = 0; i < powerlist.Count(); i++)
                    {
                        double movingArea30 = 0;
                        
                        for (int j = 0; j < 30; j++)
                        {
                            int index = i + j;
                            index %= powerlist.Count();
                            movingArea30 += powerlist[index];
                        }
                        movingArea30 /= 30;
                        dataGridView1.Rows.Add(movingArea30);
                    }  
                }
                fileParse();
                dataGridView1.Visible = true;
            }
        }


        public void fileParse()
        {
            //textfile with certain characters checking
            foreach (header a in headVal)
            {
                lineNo = a.headerline;

                switch (a.headername)
                {
                    case "Params":
                        {
                            int nextLine = lineNo + 1;
                            //seperating the values with tabs
                            string[] newline = Filelines[nextLine].Split('\t');
                            string value = newline[0];
                            int add = 0;
                            int addval = 1;
                            string[] paramsValue = new string[22];
                            do
                            {

                                foreach (char checkequal in value)
                                {
                                    if (checkequal == '=')
                                    {
                                        paramsValue[add] = value.Substring(addval, value.Length - addval);

                                    }

                                    addval++;
                                }


                                addval = 1;
                                add++;
                                nextLine++;
                                newline = Filelines[nextLine].Split('\t');
                                value = newline[0];

                            } while (nextLine < 24);//newline!=null);

                            
                            allParameters.Version = paramsValue[0];
                            allParameters.Monitor = paramsValue[1];
                            allParameters.SMode = paramsValue[2];
                            allParameters.Date = paramsValue[3];
                            allParameters.StartTime = paramsValue[4];
                            allParameters.Length = paramsValue[5];
                            allParameters.Interval = paramsValue[6];
                            allParameters.Upper1 = paramsValue[7];
                            allParameters.Lower1 = paramsValue[8];
                            allParameters.Upper2 = paramsValue[9];
                            allParameters.Lower2 = paramsValue[10];
                            allParameters.Upper3 = paramsValue[11];
                            allParameters.Lower3 = paramsValue[12];
                            allParameters.Timer1 = paramsValue[13];
                            allParameters.Timer2 = paramsValue[14];
                            allParameters.Timer3 = paramsValue[15];
                            allParameters.ActiveLimit = paramsValue[16];
                            allParameters.MaxHR = paramsValue[17];
                            allParameters.RestHR = paramsValue[18];
                            allParameters.StartDelay = paramsValue[19];
                            allParameters.VO2max = paramsValue[20];
                            allParameters.Weight = paramsValue[21];


                            //adding the getter and setter
                            label16.Text = "Version: " + allParameters.Version;
                            label17.Text = "Monitor: " + allParameters.Monitor;
                            label18.Text = "SMode: " + allParameters.SMode;
                            label19.Text = "Date: " + allParameters.Date;
                            label20.Text = "Start Time: " + allParameters.StartTime;
                            label21.Text = "Length: " + allParameters.Length;
                            label22.Text = "Interval: " + allParameters.Interval;
                            label23.Text = "Upper1: " + allParameters.Upper1;
                            label24.Text = "Upper2: " + allParameters.Upper2;
                            label25.Text = "Upper3: " + allParameters.Upper3;
                            label26.Text = "Lower1: " + allParameters.Lower1;
                            label27.Text = "Lower2: " + allParameters.Lower2;
                            label28.Text = "Lower3: " + allParameters.Lower3;
                            label29.Text = "Timer1: " + allParameters.Timer1;
                            label31.Text = "Timer2: " + allParameters.Timer2;
                            label32.Text = "Timer3: " + allParameters.Timer3;
                            label33.Text = "ActiveLimit: " + allParameters.ActiveLimit;
                            label34.Text = "MaxHR: " + allParameters.MaxHR;
                            label35.Text = "RestHR: " + allParameters.RestHR;
                            label36.Text = "StartDelay: " + allParameters.StartDelay;
                            label37.Text = "VO2max: " + allParameters.VO2max;
                            label38.Text = "Weight: " + allParameters.Weight;



                            break;
                        }
                    case "HRData":
                        {


                            //using the loop to till the end of array to get values 
                            for (int j = lineNo + 1; j < Filelines.Length; j++)
                            {
                                //Spliting chars with tabs
                                string[] newline = Filelines[j].Split('\t');


                                //Switching different versions 
                                switch (allParameters.Version)
                                {

                                    case "105":
                                        {
                                            hr.Add(new hrdata
                                            {
                                                HeartRate = int.Parse(newline[0]),
                                                Speed = int.Parse(newline[1]),
                                                Cadence = int.Parse(newline[2])
                                            });

                                            break;
                                        }
                                    case "106":
                                        {


                                            //adding the values 
                                            hr.Add(new hrdata
                                            {

                                                HeartRate = int.Parse(newline[0]),
                                                Speed = double.Parse(newline[1]) * 0.1,
                                                Cadence = int.Parse(newline[2]),
                                                Altitude = int.Parse(newline[3]),
                                                Power = int.Parse(newline[4]),
                                                PowerBalancePedalling = int.Parse(newline[5])
                                            });

                                            break;
                                        }
                                    case "107":
                                        {
                                            //adding the values to the parameters
                                            hr.Add(new hrdata
                                            {
                                                HeartRate = int.Parse(newline[1]),
                                                Speed = double.Parse(newline[2]) * 0.1,
                                                Cadence = int.Parse(newline[3]),
                                                Altitude = int.Parse(newline[4]),
                                                Power = int.Parse(newline[5]),
                                                PowerBalancePedalling = int.Parse(newline[6]),
                                                AirPressure = int.Parse(newline[7])
                                            });
                                            break;
                                        }
                                        
                                }
                            }
                            break;

                        }
                }
                
            }
            

            //putting column name is data grid view as per version of parmas
            string[] columnNames = { "Time Stamp", " HeartRate", " Speed", " Cadence", " Altitude", " Power", "PowerBalancePedalling" };


            foreach (string col in columnNames)
            {
                dt.Columns.Add(col);
            }


            foreach (hrdata hd in hr)
            {



                dt.Rows.Add(hd.TimeStamp , hd.HeartRate , hd.Speed, hd.Cadence, hd.Altitude, hd.Power, hd.PowerBalancePedalling);
            }

            //variables initiated for heart rate
            int minHeartRate = 1000;
            int maxHeartRate = 0, sum = 0;
            int count = 0;

            //variables for altitude ,power,speed

            int minAltitude = 1000;
            int maxAltitude = 0;
            int avgaltitude = 0;

            int minPower = 1000;
            int maxPower = 0;
            int avgPower = 0;

            double minSpeed = 1000;
            double maxSpeed = 0;
            double avgspeed = 0;
            //calculating the data to find maximum,minimum
            

            foreach (hrdata value in hr)
            {
                
                //for heart rate
                int hrValue = value.HeartRate;
                if (hrValue < minHeartRate)
                {
                    minHeartRate = hrValue;
                }
                else if (hrValue > maxHeartRate)
                {
                    maxHeartRate = hrValue;
                }
                sum += hrValue;
                count++;
                //calculation for altitude 
                int altValue = value.Altitude;
                if (altValue < minAltitude)
                {
                    minAltitude = altValue;
                }
                else if (altValue > maxAltitude)
                {
                    maxAltitude = altValue;
                }
                if (altValue <= minAltitude && altValue >= maxAltitude)
                {
                    avgaltitude = altValue;
                }
               

                //calculation for the power
                int PowerValue = value.Power;
                if (PowerValue < minPower)
                {
                    minPower = PowerValue;
                }
                else if (altValue > maxPower)
                {
                    maxPower = PowerValue;
                }
                if (PowerValue != 0)
                {
                    if (PowerValue <= minPower && PowerValue >= minPower)
                    {
                        avgPower = PowerValue;
                    }
                }
                

                //calculation of the speed
                double SpeedValue = value.Speed;
                if (SpeedValue < minSpeed)
                {
                    minSpeed = SpeedValue;
                }
                else if (SpeedValue > maxSpeed)
                {
                    maxSpeed = SpeedValue;
                }
                if (SpeedValue <= minSpeed && SpeedValue >= maxSpeed)
                {
                    avgspeed = SpeedValue;
                }

            }
            //calculating the stats of heart rate
            label7.Text ="Average Heart Rate: "+ (sum / count).ToString();
            label8.Text ="Maximum Heart Rate: "+maxHeartRate.ToString();
            label9.Text = "Minimum Heart Rate: " + minHeartRate.ToString();
            lb10 = label1.Text = "Average Speed: " + avgspeed.ToString() + " Miles/Hr";
            lb4 =  "Average Speed: " + (avgspeed *1.6).ToString() + " KM/Hr";
            
            lb12 = label2.Text = "Average Altitude: " + avgaltitude.ToString() + " Meter";
            lb6 = "Average Altitude: " + (avgaltitude/0.304).ToString().Substring(0, 4) + " Feet";

            label3.Text = "Average Power: " + avgPower.ToString()+ " Watt";
            
            //calculating for altitude ,power ,speed
            label10.Text ="Maximun Power: " + maxPower.ToString()+"Watt";

            lb7 = label11.Text ="Total Distance covered: " + (avgspeed * 1).ToString() + " Miles"  ;
            lb1 = "Total Distance covered: " + (avgspeed / 0.6).ToString().Substring(0,4) + " KM";


            lb11 = label12.Text = "Maximum Altitude: " + maxAltitude.ToString()+"Meter";
            double tomile = Convert.ToDouble(maxSpeed) * 0.62137119;
            lb5 = "Maximum Altitude: " + (maxAltitude/0.304).ToString().Substring(0,4) + "Feet";

            lb8 = label14.Text = "Maximum Speed: " + (maxSpeed+tomile).ToString()+" Miles/Hr";
            lb2 = "Maximum Speed: " + (maxSpeed).ToString() + " KM/Hr";

            lb9 = label15.Text = "Minimum Speed: " + minSpeed.ToString() + " Miles/Hr";
            lb3 = "Minimum Speed: " + (minSpeed*1.6).ToString() + " KM/Hr";

            cycleInfo.DataSource = dt;
        }
        

        private void ParseData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void ParseData_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void staticGraphToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            graph gr = new graph();
            gr.setHR(hr);
            gr.Show();
        }

        private void tabControl1_Click(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            label11.Text = lb1.ToString();
            label14.Text = lb2.ToString();
            label15.Text = lb3.ToString();
            label1.Text = lb4.ToString();
            label12.Text = lb5.ToString();
            label2.Text = lb6.ToString();
            
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            label11.Text = lb7.ToString();
            label14.Text = lb8.ToString();
            label15.Text = lb9.ToString();
            label1.Text = lb10.ToString();
            label12.Text = lb11.ToString();
            label2.Text = lb12.ToString();
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            
        }
   
    }

}
