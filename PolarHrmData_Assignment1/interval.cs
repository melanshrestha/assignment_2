﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolarHrmData_Assignment1
{
    public partial class interval : Form
    {
        public interval()
        {
            InitializeComponent();
        }

        string file_name;

        public void getfile_name(string file)
        {
            this.file_name = file;
        }

        private void interval_Load(object sender, EventArgs e)
        {
            int[] GEPPWR;

            int[] GEPPWR2;
            int[] GEPPWR3;
            int[] GEPPWR4;

            try
            {
                string txtData = File.ReadAllText(file_name);
                string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                int index2 = Array.IndexOf(arrData, "[HRData]");

                GEPPWR = new int[arrData.Length - (index2 + 1)];

                int j = 0;
                double powertotal = 0;

                for (int i = index2 + 1; i < 147; i++)
                {
                    string HRData = arrData[i];
                    string[] arrHrdata = Regex.Split(HRData, @"\W+");

                    GEPPWR[j] = Convert.ToInt32(arrHrdata[4]);

                    //Total power1
                    double pwr = GEPPWR[j];
                    powertotal = powertotal + pwr;

                    j++;
                }

                //for average power1
                double avgpower1 = Math.Round(powertotal / j);
                this.label1.Text = avgpower1.ToString();

                //for maximum Power1
                int maxpwr1 = GEPPWR.Max();
                this.label2.Text = maxpwr1.ToString();

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }




            try
            {
                string txtData = File.ReadAllText(file_name);
                string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                int index2 = Array.IndexOf(arrData, "[HRData]");

                GEPPWR2 = new int[arrData.Length - (index2 + 1)];

                int a = 0;
                double powertotal2 = 0;

                for (int i = index2 + 35; i < 723; i++)
                {
                    string HRData = arrData[i];
                    string[] arrHrdata = Regex.Split(HRData, @"\W+");

                    GEPPWR2[a] = Convert.ToInt32(arrHrdata[4]);

                    //Total power2
                    double pwr2 = GEPPWR2[a];
                    powertotal2 = powertotal2 + pwr2;
                    a++;
                }

                //for average power2
                double avgpower2 = Math.Round(powertotal2 / a);
                this.label3.Text = avgpower2.ToString();

                //for maximum Power2
                int maxpwr2 = GEPPWR2.Max();
                this.label4.Text = maxpwr2.ToString();

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            //for 3rd Group
            try
            {
                string txtData = File.ReadAllText(file_name);
                string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                int index2 = Array.IndexOf(arrData, "[HRData]");

                GEPPWR3 = new int[arrData.Length - (index2 + 1)];

                int b = 0;
                double powertotal3 = 0;

                for (int i = index2 + 608; i < 1339; i++)
                {
                    string HRData = arrData[i];
                    string[] arrHrdata = Regex.Split(HRData, @"\W+");

                    GEPPWR3[b] = Convert.ToInt32(arrHrdata[4]);

                    //Total power3
                    double pwr3 = GEPPWR3[b];
                    powertotal3 = powertotal3 + pwr3;
                    b++;
                }

                //for average power3
                double avgpower3 = Math.Round(powertotal3 / b);
                this.label5.Text = avgpower3.ToString();

                //for maximum Power2
                int maxpwr3 = GEPPWR3.Max();
                this.label6.Text = maxpwr3.ToString();

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            //for 4th Group
            try
            {
                string txtData = File.ReadAllText(file_name);
                string[] arrData = txtData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                int index2 = Array.IndexOf(arrData, "[HRData]");

                GEPPWR4 = new int[arrData.Length - (index2 + 1)];

                int d = 0;
                double powertotal4 = 0;

                for (int i = index2 + 1510; i < 2245; i++)
                {
                    string HRData = arrData[i];
                    string[] arrHrdata = Regex.Split(HRData, @"\W+");

                    GEPPWR4[d] = Convert.ToInt32(arrHrdata[4]);

                    //Total power1
                    double pwr4 = GEPPWR4[d];
                    powertotal4 = powertotal4 + pwr4;
                    d++;
                }

                //for average power2
                double avgpower4 = Math.Round(powertotal4 / d);
                this.label7.Text = avgpower4.ToString();

                //for maximum Power2
                int maxpwr4 = GEPPWR4.Max();
                this.label8.Text = maxpwr4.ToString();

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
